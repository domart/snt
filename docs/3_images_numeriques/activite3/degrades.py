image = open('./images/degrade0.ppm', 'w')

image.write('P3\n')
image.write('256 50\n')
image.write('255\n')

# On ajoute 50 lignes
for ligne in range(50):
    for colonne in range(256):
        image.write(f'255 {colonne} 0')
        image.write('\n')

image.close()

#########################################

image = open('./images/degrade1.ppm', 'w')

image.write('P3\n')
image.write('256 256\n')
image.write('255\n')

# On ajoute 50 lignes
for ligne in range(256):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(256):
        image.write(f'255 {ligne} {colonne}')
        image.write('\n')

image.close()

#########################################

image = open('./images/degrade2.ppm', 'w')

image.write('P3\n')
image.write('256 256\n')
image.write('255\n')

# On ajoute 50 lignes
for ligne in range(256):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(256):
        image.write(f'0 {ligne} {colonne}')
        image.write('\n')

image.close()
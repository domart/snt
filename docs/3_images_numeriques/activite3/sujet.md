---
author: "Benoît Domart"
title: "Activité 3 - Des images ... en couleur ! (PPM)"
---

## Cours - Des images en couleurs

???+abstract "Cours - Des images en couleurs"

    Pour comprendre comment représenter les images en couleurs, il faut d'abord s'intéresser à la manière dont notre œil perçoit ces dernières.

    Notre œil contient des  cellules, les cônes, qui sont sensibles à la couleur, c'est-à-dire à la longueur d'onde de la lumière qu'ils reçoivent. Ces cônes sont de trois sortes, dont le  maximum de sensibilité est respectivement dans le rouge, le vert et le bleu. Quand notre œil reçoit une lumière monochrome émise par une ampoule jaune, les cônes sensibles au rouge et au vert réagissent beaucoup et ceux sensibles au bleu un tout petit peu, exactement comme s'il recevait un mélange de  lumières émises par deux ampoules rouge et verte. Ainsi, en mélangeant de la lumière produite par une ampoule rouge et une ampoule verte, on peut donner à l'œil la  même sensation que s'il recevait une lumière jaune. Plus généralement, quelle que soit la lumière qu'il reçoit, notre œil ne communique à notre cerveau qu'une information partielle : l'intensité de la réaction des cônes sensibles au rouge, au vert et au bleu. Et deux lumières qui stimulent ces trois types de cônes de manière identique sont indiscernables pour l'œil.

    Ainsi, sur l'écran d'un ordinateur, chaque pixel est composé non pas d'une, mais de trois sources de lumière, rouge, verte et bleue ; en faisant varier l'intensité  de chacune de ces sources, on peut simuler n'importe quelle couleur. Par exemple, en mélangeant de la lumière verte et de la lumière bleue on obtient de la lumière  Cyan. En mélangeant de la lumière rouge et de la lumière bleue on obtient de la lumière magenta. Et en mélangeant de la lumière rouge et de la lumière verte on  obtient de la lumière jaune. *Cyan* est le nom savant d'un bleu turquoise et *magenta* celui d'un rouge tirant un peu sur le violet.

## Cours - La synthèse additive

???+abstract "Cours - La synthèse additive"

    <center>
        ![type:video](https://www.youtube-nocookie.com/embed/IMdL0p6JGF4)
    </center>

    La synthèse additive des couleurs est le procédé consistant à combiner les lumières de plusieurs sources colorées dans le but d'obtenir une lumière colorée quelconque dans un gamut déterminé.

    La synthèse additive utilise généralement trois lumières colorées : une rouge, une verte et une bleue (RVB ou **RGB** en anglais pour *red, green, blue*). L'addition de ces trois lumières colorées en proportions convenables donne la lumière blanche. L'absence de lumière donne du noir :

    <center>
        ![](./images/Synthese-additive.png)
    </center>

    Les écrans d'ordinateur fonctionne ainsi. Chaque pixel n'est en fait pas constitué d'une seule valeur (le niveau de gris), mais de trois valeurs :
    
    - le niveau de **rouge**,
    - le niveau de **vert**,
    - le niveau de **bleu**.

    Ces trois valeurs sont des entiers entre 0 et 255. La couleur d'un pixel est donnée par le triplet formé par ces trois nombres. Par exemple :

    - Noir correspond à (0, 0, 0),
    - Rouge correspond à (255, 0, 0),
    - Vert correspond à (0, 255, 0),
    - Bleu correspond à (0, 0, 255),
    - Jaune correspond à (255, 255, 0),
    - Magenta correspond à (255, 0, 255),
    - Cyan correspond à (0, 255, 255),
    - Blanc correspond à (255, 255, 255),
    - ...

    Vous pouvez retrouver le triplet correspondant à une couleur donnée en utilisant par exemple l'outil fourni [ici](https://www.w3schools.com/colors/colors_rgb.asp){target=_blank}.

    *Source : [Wikipedia](https://fr.wikipedia.org/wiki/Synthèse_additive){target=_blank}*

## Cours - Le format PPM

???+abstract "Cours - Le format PPM"

    Un format, parmi d'autres, pour exprimer ces images est le format [PPM](https://fr.wikipedia.org/wiki/Portable_pixmap#PPM){target=_blank} (*Portable PixMap*). Pour exprimer une image dans ce format, on choisit une valeur maximale, par exemple 255, pour exprimer l'intensité des couleurs et on associe à chaque pixel trois nombres, l'intensité en rouge, en vert et en bleu, chaque nombre étant compris entre 0 et 255. Un fichier au format PPM, ressemble beaucoup à un fichier au format PBM ou PGM ; c'est un fichier ASCII qui se compose comme suit :

    - les caractères `P3`, suivis d'un retour à la ligne ou d'un espace,
    - la largeur de l'image, suivie d'un retour à la ligne ou d'un espace,
    - la hauteur de l'image, suivie d'un retour à la ligne ou d'un espace,
    - la valeur maximale utilisée pour exprimer l'intensité des couleurs (on prendra toujours  255),
    - la liste des valeurs des couleurs, trois par pixel, **dans l'ordre rouge, vert, bleu**, ligne par ligne, de haut en bas et de gauche à droite, séparées par des retours à la ligne ou des espaces.

???+example "Une image au format PPM"

    ```
    P3
    # Le P3 signifie que les couleurs sont en ASCII, et qu'elles sont en RGB.
    # L'image contient 3 colonnes et 2 lignes:
    3 2
    # Ayant 255 pour valeur maximum:
    255

    # On indique ensuite sur chaque ligne un triple, pour le rouge, le vert et le bleu.
    255 0   0
    0   255 0
    0   0   255
    255 255 0
    255 255 255
    0   0   0
    ```

    On obtient l'image suivante :

    <center>
        ![](./images/exemple-PPM.png)
    </center>

## Exercices

???+exercice "Exercice 1"

    Écrire un script Python permettant de reproduire l'image suivante ci-dessous. Il s'agit d'un rectangle de 255 colonnes par 50 lignes. La valeur du rouge est toujours égale à 255, celle du bleu est toujours égale à 0, et celle du vert varie en fonction deu numéro de la colonne, ce qui permet d'obtenir un dégradé.

    <center>
        ![](./images/degrade0.png){width=530}
    </center>

    Les 50 lignes de l'image sont donc identiques.

    On pourra s'aider de l'aide juste ci-dessous si besoin !

???+hint "Aide !"

    En Python, il est facile d'avoir une chaîne de caractères dont la valeur varie en fonction de la valeur d'une variable.

    Par exemple, si on a une boucle `#!python for` dont la variable s'appelle `colonne`, et qu'on souhaite avoir la chaîne de caractères composée du nombre 255, de la valeur de la variable `colonne`, et enfin du nombre 255, on écrira
    ```python
    f'255 {colonne} 255'
    ```
    Il doit y avoir un `f` devant les apostrophes définissant la chaîne de caractères, et la variable doit être à l'intérieur d'accolades.

???+exercice "Exercice 2"

    Écrire un script Python permettant de reproduire l'image suivante ci-dessous. Il s'agit d'un carré de taille 255 par 255. La valeur du rouge est toujours égale à 255, et celles du vert et du bleu varient en fonction de l'emplacement dans l'image, ce qui permet d'obtenir un dégradé.

    <center>
        ![](./images/degrade1.png){width=400}
    </center>

???+exercice "Exercice 3"

    Cet exercice a pour but de concevoir le dégradé ci-dessous de 100 pixels de hauteur et composé de 1280 couleurs, du rouge au violet (en fait, il n'y aura que 1276 couleurs différentes si on chipote) :

    <center>
        ![](./images/degrade3.png){width=1000}
    </center>

    Pour cela, on étudie une représentation graphique possible des composantes R, G, B dans un repère du plan pour un tel dégradé où deux composantes restent constantes tandis que la 3ème varie de 0 à 255 ou de 255 à 0.

    <center>
        ![](./images/degrade3-explications.png){width=1000}
    </center>
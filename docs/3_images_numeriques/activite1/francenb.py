# On créé un fichier francenb2.pbm.
image = open('francenb2.pbm', 'w')

image.write('P1\n')
image.write('30 20\n')

# On ajoute 20 lignes
for ligne in range(20):
    # Les 10 premiers nombres sont des 1.
    for colonne in range(10):
        image.write('1 ')
    # Les 10 suivants sont des 0.
    for colonne in range(10):
        image.write('0 ')
    # Et on termine chaque ligne avec 
    for colonne in range(10):
        image.write('1 ')
    image.write('\n')

image.close()
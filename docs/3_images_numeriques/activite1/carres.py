import os

os.chdir('/home/ben/Documents/GIT_Projets/domart/snt/docs/2_images_numeriques/activite1/images/')

######################

image = open('carre.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if ligne == 0 or ligne == 48 or colonne == 0 or colonne == 48:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('chess.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if (colonne + ligne) % 2 == 0:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('quadrillage.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if colonne % 2 == 0 or ligne % 2 == 0:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('cercle2.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if (colonne-25)**2 + (ligne-25)**2 - 20**2 <= 1:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('carre-noir-blanc.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if ligne <= 15 or ligne >= 32 or colonne <= 15 or colonne >= 32:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('carre-blanc-noir.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if 16 <= ligne <= 31 and 16 <= colonne <= 31:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

######################

image = open('carres-inscrits.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if (ligne%2 == 0 and (ligne <= colonne <= 48-ligne or 48-ligne <= colonne <= ligne)) or (colonne%2 == 0 and (colonne <= ligne <= 48-colonne or 48-colonne <= ligne <= colonne)):
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()
image = open('./images/diagonale.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne.
    for colonne in range(49):
        if ligne == colonne:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

#########################################

image = open('./images/croix.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne.
    for colonne in range(49):
        if ligne == colonne or ligne+colonne == 48:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

#########################################

image = open('./images/diagonale3.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if ligne == colonne or ligne == colonne-1 or ligne == colonne+1:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

#########################################

image = open('./images/losange.pbm', 'w')

image.write('P1\n')
image.write('49 49\n')

# On ajoute 50 lignes
for ligne in range(49):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(49):
        if ligne+colonne == 24 or colonne-ligne == 24 or ligne-colonne == 24 or ligne+colonne == 72:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()
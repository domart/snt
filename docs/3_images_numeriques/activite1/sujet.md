---
author: "Benoît Domart"
title: "Activité 1 - Des premières images, en noir & blanc (PBM)"
---

## Cours - Images matricielles et pixels

???+abstract "Cours - Images matricielles et pixels"
    Nous allons ici découvrir un premier format d'image numérique.

    Pour décrire l'image **1**, une possibilité est de dire : « cette image est formée d'un cercle ». On peut même être plus précis et indiquer les coordonnées du centre du cercle et son rayon. Et, à partir de cette description, n'importe qui pourrait reconstituer le dessin.

    <center>
        ![](./images/image1.png){width=20%}
    </center>

    On peut donc représenter cette image par trois nombres : deux pour les coordonnées du centre et un pour le rayon. Une image formée de plusieurs cercles serait de même décrite par trois nombres pour chacun d'eux. On peut représenter d'une manière similaire un dessin formé de cercles et de rectangles, en représentant chaque figure par une lettre, « c » pour un cercle, « r » pour un rectangle, suivi d'une suite de nombres qui définissent les paramètres de la figure. On parle alors de **représentation symbolique**, ou parfois de **représentation vectorielle**, d'une image, ou tout simplement d'une **[image vectorielle](https://fr.wikipedia.org/wiki/Image_vectorielle){target=_blank}**.

    Néanmoins, cette méthode est peu pratique pour représenter l'image **2**.

    <center>
        ![](./images/image2.png){width=20%}
    </center>

    Une autre méthode, qui a l'avantage de pouvoir être utilisée sur n'importe quelle image, consiste à superposer un quadrillage à l'image **3**.

    <center>
        ![](./images/image3.png){width=20%}
    </center>

    Chacune des cases de ce quadrillage s'appelle un [pixel](https://fr.wikipedia.org/wiki/Pixel){target=_blank} (*picture element*). On noircit ensuite les pixels qui contiennent une portion de trait (**4**). Puis, il suffit d'indiquer la couleur de chacun des pixels, en les lisant de gauche à droite et de haut en bas, comme un texte. Ce dessin se décrit donc par une suite de mots « blanc » ou « noir ». Comme seuls les mots « noir » ou « blanc » sont utilisés, on peut être plus économe et remplacer chacun de ces mots par un bit, par exemple *1* pour « *noir* » et *0* pour « *blanc* ».

    <center>
        ![](./images/image4.png){width=20%}
    </center>

    Le dessin ci-avant, avec une grille de 10 × 10, se décrit alors par la suite de 100 bits suivante :
    ``` title="Une première image - Un cercle"
    0000000000001111110001100001100100000010010000001001000000100100000010011000011000111111000000000000
    ```

    Cette description est assez approximative, mais on peut la rendre plus précise en utilisant un quadrillage, non plus de 10 × 10 pixels, mais de 100 × 100 pixels. À partir de quelques millions de pixels, notre œil n'est plus capable de faire la différence entre les deux images. Cette manière de représenter une image sous la forme d'une suite de pixels, chacun exprimés sur un bit, s'appelle une *[bitmap](https://fr.wikipedia.org/wiki/Windows_bitmap){target=_blank}*. *Bitmap* signifie *tableau de bits*. C'est une méthode approximative, mais universelle : n'importe quelle image en noir et blanc peut se décrire ainsi.

    On parle alors d'**[images matricielles](https://fr.wikipedia.org/wiki/Image_matricielle){target=_blank}**, *matrice* signifiant ici *tableau*.

    Les différents formats de photos numériques que vous connaissez, comme PNG ou JPEG, sont des formats d'images matricielles. Nous ne nous intéresserons ici qu'à ce type de représentation d'une image.

## Cours - Le format d'image PBM

???+abstract "Cours - Le format d'image PBM"
    
    Nous allons donc créer un fichier, qui va contenir la suite de bits suivante, qui représente notre image :
    ``` title="Une première image - Un cercle"
    0000000000001111110001100001100100000010010000001001000000100100000010011000011000111111000000000000
    ```

    Mais il n’est pas *a priori* évident que cette suite de 100 pixels exprime une image de 10 × 10 pixels. Il pourrait aussi s’agir par exemple d’un texte en ASCII, ou d'un fichier de musique, ... Pour cette raison, au lieu d’appeler le fichier simplement 📄`cercle`, on l’appelle 📄`cercle.pbm`. Cette **extension** *pbm* du nom indique que ce fichier est exprimé dans le format [PBM](https://fr.wikipedia.org/wiki/Portable_pixmap#PBM){target=_blank} (*Portable BitMap*). Ce format est l’un des plus simples pour exprimer des images. Un fichier au format PBM est un fichier en ASCII qui se compose comme suit :

    - Les caractères `P1`, suivis d’un retour à la ligne ou d’un espace,
    - La largeur de l’image, en base 10, suivie d’un retour à la ligne ou d’un espace,
    - La hauteur de l’image, en base 10, suivie d’un retour à la ligne ou d’un espace,
    - La liste des pixels, ligne par ligne, de haut en bas et de gauche à droite, séparés par des espaces.
    
        - Un 1 signifie "noir".
        - Un 0 signifie "blanc".
    - Les retours à la ligne et les espaces sont ignorés dans cette partie.
    - On peut également ajouter des *commentaires*, c'est-à-dire du texte qui n'est là que pour nous humain, et qui est ignoré par l'ordinateur lorsqu'on ouvre le fichier en tant qu'image.
    - Enfin, aucune ligne ne doit dépasser 70 caractères.

    En résumé, notre fichier va donc contenir le texte suivant :

    ``` title="📄cercle.pbm"
    P1
    # Mon premier fichier PBM : cercle
    10 10
    0 0 0 0 0 0 0 0 0 0 
    0 0 1 1 1 1 1 1 0 0 
    0 1 1 0 0 0 0 1 1 0 
    0 1 0 0 0 0 0 0 1 0 
    0 1 0 0 0 0 0 0 1 0 
    0 1 0 0 0 0 0 0 1 0 
    0 1 0 0 0 0 0 0 1 0 
    0 1 1 0 0 0 0 1 1 0 
    0 0 1 1 1 1 1 1 0 0 
    0 0 0 0 0 0 0 0 0 0 
    ```


## Exercice 1 - Une première image en noir & blanc

???+exercice "Exercice 1 - Une première image en noir & blanc"

    Nous allons créer un fichier image représentant le cercle ci-dessus.

    Pour cela, dans votre espace personnel :

    1. Dans votre dossier 📂`SNT`, créer un sous-dossier 📂`2-ImagesNumeriques`, puis dans ce nouveau dossier, créer un sous-dossier 📂`Activite1`.
    2. Dans ce dossier 📂`Activite1`, créer un nouveau fichier texte appelé 📄`cercle.pbm` (en faisant un clic droit avec la souris, puis `Nouveau > Document texte`, et enfin en le renommant comme indiqué).
    3. Ouvrir ce fichier avec un éditeur de texte (le *Bloc-notes* par exemple).
    4. Copier le code indiqué ci-dessus dans le fichier et l'enregistrer.
    5. Sous windows, *Paint* ne sait pas afficher ce typer d'image. Il faut soit utiliser *Gimp* s'il est installé sur l'ordinateur, soit utiliser un éditeur en ligne pour afficher l'image, par exemple [https://kylepaulsen.com/stuff/NetpbmViewer/](https://kylepaulsen.com/stuff/NetpbmViewer/){target=_blank}.



## Exercice 2 - Les drapeaux français et suisse en noir & blanc

???+exercice "Exercice 2 - Les drapeaux français et suisse en noir & blanc"

    Créer deux fichiers 📄`francenb.pbm` et 📄`suissenb.pbm` qui contiendront les drapeaux de ces deux pays en noir et blanc.

    1. Pour la France : 30 colonnes et 20 lignes - ![](./images/francenb.png)
    1. Pour la Suisse : 50 colonnes et 50 lignes - ![](./images/suissenb.png)


## Exercices - Génération des fichiers avec Python

???+exercice "Exercice 3 - Génération des fichiers avec Python"

    Lorsque les fichiers deviennent gros, il devient rapidement fastidieux de les écrire *à la main*. Pour automatiser le processus, on peut créer un script Python !

    Le script suivant permet de générer le fichier contenant le drapeau français :

    ``` py title="🐍 Script Python 📄francenb2.py" linenums="1"
    # On créé un fichier francenb2.pbm.
    image = open('francenb2.pbm', 'w')

    image.write('P1\n')
    image.write('30 20\n')

    # On ajoute 20 lignes
    for ligne in range(20):
        # Les 10 premiers nombres sont des 1.
        for colonne in range(10):
            image.write('1 ')
        # Les 10 suivants sont des 0.
        for colonne in range(10):
            image.write('0 ')
        # Et on termine chaque ligne avec 
        for colonne in range(10):
            image.write('1 ')
        # On fini avec un retour chariot pour passer à la ligne dans le prochain tour de la boucle
        image.write('\n')

    # On enregistre tout ce qu'on a fait dans le fichier
    image.close()
    ```

???+exercice "Exercice 4 - Génération des fichiers avec Python"

    Compléter le script Python ci-dessous pour générer le drapeau suisse :

    ``` py title="🐍 Script Python 📄suissenb2.py" hl_lines="5 25 28 31" linenums="1"
    # On créé un fichier francenb2.pbm.
    image = open('suissenb2.pbm', 'w')

    image.write('P1\n')
    image.write('... ...\n')

    # Sur les 10 premières lignes, il n'y a que du noir :
    for ligne in range(10):
        for colonne in range(50):
            image.write('1 ')
        image.write('\n')

    # Sur les 10 lignes suivantes, il y a du noir, puis du blanc, puis du noir,
    # c'est le haut de la croix.
    for ligne in range(10):
        for colonne in range(20):
            image.write('1 ')
        for colonne in range(10):
            image.write('0 ')
        for colonne in range(20):
            image.write('1 ')
        image.write('\n')

    # Il faut ajouter le milieu de la croix ...


    # ... puis le bas de la croix ...


    # ... puis le bas de l'image qui ne contient à nouveau que du noir !


    # On enregistre tout ce qu'on a fait dans le fichier
    image.close()
    ```

???+exercice "Exercice 5"

    1. Écrire un script Python qui génère une image d'une taille de 49 par 49, contenant uniquement la diagonale.
    2. Écrire un script Python qui génère une image d'une taille de 49 par 49, contenant les deux diagonales (et faisant donc une croix).
    3. Écrire un script Python qui génère une image d'une taille de 49 par 49, contenant une diagonale d'épaisseur de trois lignes.
    4. Écrire un script Python qui génère une image d'une taille de 49 par 49, contenant un losange dont les sommets sont les milieux de chaque bord.

    <center>
        ![](./images/diagonale.png){width=150}
        ![](./images/croix.png){width=150}
        ![](./images/diagonale3.png){width=150}
        ![](./images/losange.png){width=150}
    </center>


???+exercice "Exercice 6"

    Amuse toi à générer les images en noir et blanc que tu veux !

    Tu peux déposer les images que tu as créées [ici](https://e-education.recia.fr/moodle/mod/assign/view.php?id=1631070){target=_blank}.


???+exercice "Exercice 7"

    1. Écrire un script Python permettant de générer une image contenant une ligne sur deux en noir, et une sur deux en blanc.

        ???+tip "Aide - Une ligne sur 2"

            Pour écrire une ligne sur 2, on peut utiliser le test

            ```py
            if ligne%2 == 0:
            ```

            `ligne%2` permet d'obtenir le reste de la division euclidienne de `ligne` par `2`. Ainsi,

            - `ligne%2 == 0` est vrai si le numéro de ligne est pair ;
            - `ligne%2 == 0` est faux si le numéro de ligne est impair.


    1. Écrire un script Python permettant de générer un damier d'échec (c'est-à-dire qu'une case sur 2 est blanche, et une sur deux est noire).

???+exercice "Exercice 8 - Pour s'entraîner pour le DS"

    1. Écrire un script Python permettant de générer l'image au format PBM suivante :

        <center>
            ![](./images/carre-noir-blanc.png){width=150}
        </center>

    1. Écrire un script Python permettant de générer l'image au format PBM suivante :

        <center>
            ![](./images/carre-blanc-noir.png){width=150}
        </center>

???+exercice "Exercice 9 - 💪💪"

    Écrire un script Python permettant de générer l'image au format PBM suivante :

    <center>
        ![](./images/carres-inscrits.png){width=150}
    </center>
image = open('image2py.pbm', 'w')

image.write('P1\n')
image.write('50 50\n')

for ligne in range(10):
    for colonne in range(50):
        image.write('1 ')
    image.write('\n')

for ligne in range(10):
    for colonne in range(20):
        image.write('1 ')
    for colonne in range(10):
        image.write('0 ')
    for colonne in range(20):
        image.write('1 ')
    image.write('\n')

for ligne in range(10):
    for colonne in range(10):
        image.write('1 ')
    for colonne in range(30):
        image.write('0 ')
    for colonne in range(10):
        image.write('1 ')
    image.write('\n')

for ligne in range(10):
    for colonne in range(20):
        image.write('1 ')
    for colonne in range(10):
        image.write('0 ')
    for colonne in range(20):
        image.write('1 ')
    image.write('\n')

for ligne in range(10):
    for colonne in range(50):
        image.write('1 ')
    image.write('\n')

image.close()
---
author: "Benoît Domart"
title: "Activité 2 - Des images en gris (PGM)"
---

## Cours - Des images en gris

???+abstract "Cours - Des images en gris - Le format PGM"
    
    Nous allons maintenant créer un fichier pouvant contenir du noir et du blanc, mais aussi n'importe quel niveau de gris (du plus foncé au plus clair). Les fichiers dans ce tel format, appelé [PGM](https://fr.wikipedia.org/wiki/Portable_pixmap#PGM){target=_blank} (*Portable GreyMap*) auront la structure suivante :

    - Les caractères `P2`, suivis d’un retour à la ligne ou d’un espace,
    - La largeur de l’image, en base 10, suivie d’un retour à la ligne ou d’un espace,
    - La hauteur de l’image, en base 10, suivie d’un retour à la ligne ou d’un espace,
    - Le niveau de gris maximum de l'image,
    - La liste des pixels, ligne par ligne, de haut en bas et de gauche à droite, séparés par des espaces.
    
        - Un 0 signifie "noir".
        - Le niveau maximum de gris indiqué juste avant signifie "blanc".
        - Un nombre entre ces deux nombres signifie gris, qui sera d'autant plus foncé que ce nombre est proche de 0, et d'autant plus clair que ce nombre est proche du niveau maximum (le blanc).
    - Les retours à la ligne et les espaces sont ignorés dans cette partie.
    - On peut également ajouter des *commentaires*, c'est-à-dire du texte qui n'est là que pour nous humain, et qui est ignoré par l'ordinateur lorsqu'on ouvre le fichier en tant qu'image.
    - Enfin, aucune ligne ne doit dépasser 70 caractères.

    Par exemple, si on souhaite avoir 3 niveaux de gris (en plus du noir et du blanc), on peut indiquer 4 à la troisième ligne. On aura en effet alors :

    - 0 : correspond à noir
    - 1 : correspond à gris foncé
    - 2 : correspond à gris
    - 3 : correspond à gris clair
    - 4 : correspond à blanc

    Exemple sur la page Wikipedia :

    ```
    P2
    # Affiche le mot "FEEP" (exemple de la page principale de Netpbm à propos de PGM).
    24 7
    15   # Cette ligne signifie que le blanc sera codé par "15".
    0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
    0  3  3  3  3  0  0  7  7  7  7  0  0 11 11 11 11  0  0 15 15 15 15  0
    0  3  0  0  0  0  0  7  0  0  0  0  0 11  0  0  0  0  0 15  0  0 15  0
    0  3  3  3  0  0  0  7  7  7  0  0  0 11 11 11  0  0  0 15 15 15 15  0
    0  3  0  0  0  0  0  7  0  0  0  0  0 11  0  0  0  0  0 15  0  0  0  0
    0  3  0  0  0  0  0  7  7  7  7  0  0 11 11 11 11  0  0 15  0  0  0  0
    0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
    ```

    Donnera :
    <center>
        ![](./images/Feep_pgm_example.png)
    </center>


## Exercices

???+exercice "Exercice 1 - Une image en nuances de gris"

    1. Dans votre dossier 📂`SNT / 2-ImagesNumeriques`, créer un sous-dossier 📂`Activite2`.
    1. Reproduire cette magnifique image : ![](./images/SNT.png){width=200}

???+exercice "Exercice 2 - Un dégradé"

    Écrire un programme python qui génère une image carrée, de 255 pixels (en largeur) par 100 (en hauteur), et qui affiche le dégradé suivant :

    <center>
        ![](./images/degrade.png)
    </center>

    On pourra s'aider de l'aide juste ci-dessous si besoin !

???+tip "Aide !"

    En Python, il est facile d'avoir une chaîne de caractères dont la valeur varie en fonction de la valeur d'une variable.

    Par exemple, si on a une boucle `#!python for` dont la variable s'appelle `colonne`, et qu'on souhaite avoir à chaque tour de boucle la chaîne de caractères composée du numéro de la colonne suivi d'un espace, on écrira
    ```python
    f'{colonne} '
    ```
    Il doit y avoir un `f` devant les apostrophes définissant la chaîne de caractères, et la variable doit être à l'intérieur d'accolades.

    Ici, comme on souhaite avoir une intensité égale au numéro de la colonne, on pourra par exemple écrire
    ```python
    for colonne in range(255):
        image.write(f'{colonne} ')
    ```

???+exercice "Exercice 3 - Un dégradé inversé"

    Écrire un programme python qui génère une image carrée, de 255 pixels (en largeur) par 100 (en hauteur), et qui affiche le dégradé suivant (le contraire du précédent) :

    <center>
        ![](./images/degrade-inverse.png)
    </center>
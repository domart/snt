image = open('./images/degrade.pgm', 'w')

image.write('P2\n')
image.write('256 100\n')
image.write('255\n')

# On ajoute 50 lignes
for ligne in range(100):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(256):
        image.write(f'{colonne} ')
    image.write('\n')

image.close()


###############

image = open('./images/degrade-inverse.pgm', 'w')

image.write('P2\n')
image.write('256 100\n')
image.write('255\n')

# On ajoute 50 lignes
for ligne in range(100):
    # On met un 1 si et seulement si le numéro de la ligne est le même
    # que celui de la colonne, ou un de moins ou un de plus.
    for colonne in range(256):
        image.write(f'{255-colonne} ')
    image.write('\n')

image.close()

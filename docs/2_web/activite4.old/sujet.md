---
author: "Benoît Domart"
title: "Activité 4 - HTML & CSS"
---

???+exercice "Exercice 1 - Code HTML"

    Écrire le code HTML permettant d'obtenir la page Web ci-dessous. Les liens renvoient vers la page wikipedia du chaque livre.

    <center>![](./images/prixLivreHtml.png)</center>

???+exercice "Exercice 2 - Code CSS"

    Ajouter le code CSS pour obtenir la page Web suivante :

    <center>![](./images/prixLivreCss.png)</center>

    !!!note "Rappel"

        Pour activer le CSS, si le fichier `style.css` est dans le même dossier que le fichier Html, il faut ajouter la ligne suivante, dans la partie `<head>` du fichier Html :

        ```html
        <link rel="stylesheet" href="style.css"/>
        ```

???+exercice "Exercice 3 - Tableau HTML"

    1. Écrire le code HTML permettant d'obtenir la page Web ci-dessous.

        <center>![](./images/tableauHtml.png)</center>

        !!!note "Remarque"
        
            Pour indiquer que la première ligne du tableau correspond au titre, on pourra utiliser des balises `<th>...</th>` à la place des balises `<td>...</td>` - dans la première ligne uniquement.
        
        ???success "Aide pour écrire un tableau"

            Voici le début des trois premières lignes du tableau. Il faut les compléter !

            ```html
            <table>
                <tr>
                    <th>Nom</th>
                    <th>Diamètre</th>
                </tr>
                <tr>
                    <td>Mercure</td>
                    <td>4,879</td>
                </tr>
                <tr>
                    <td>Vénus</td>
                    <td>12,104</td>
                </tr>
            </table>
            ```
    
    2. Ajouter le code CSS permettant d'obtenir la page Web ci-dessous.

        <center>![](./images/tableauHtmlCss.png)</center>

???+exercice "Exercice 4 - Votre site Web"

    Créer votre propre site Web, avec le contenu de votre choix.
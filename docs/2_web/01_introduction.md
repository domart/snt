---
author: "Benoît Domart"
title: "Activité 1 - Introduction"
---

Après avoir vu cette [vidéo de présentation](https://www.youtube.com/watch?v=GqD6AiaRo3U){target=_blank}, répondre dans votre livret aux questions suivantes :

1. En quelques mots, quelle est la différence entre **Internet** et **le Web** ?
2. Comment les sites Web sont-ils reliés entre eux ?
3. Une page Web est constituée de code HTML et de code CSS. Quels sont leurs rôles ?

---
author: "Benoît Domart"
title: "Activité 3 - Le site du port de Plouhinec"
---

Cette activité a pour but de découvrir comment sont crées les pages web. Chaque page web est en fait un programme codé en langage HTML.

Cliquer ici pour récupérer les différents fichier :

<center>
    [PortPlouhinec.zip](./PortPlouhinec.zip){ .md-button download="PortPlouhinec.zip" }
</center>

???+success "Contenu du ZIP"
	Ce fichier ZIP contient :

	- Un dossier 📂`Images` : Il contient les images utilisées dans le site.
	- Un dossier 📂`Styles` : Il contient les feuilles de styles (CSS) utilisées pour mettre en forme le site Web.
	- Trois fchiers HTML :
		
		- 📄`contacts.html`
		- 📄`index.html`
		- 📄`visite.html`
		
	Ce sont les trois pages qui constituent le site Web.

Après avoir dézippé le ZIP dans votre dossier 📂`snt/web/activite3`, ouvrir la page 📄`index.html` dans un navigateur, et ouvrir les trois pages HTML dans un éditeur de texte (Notepad ++ par exemple).

## 1. Modification du code HTML

???+exercice "Exercice 1 - Modification du code HTML"
	1. Dans les trois pages, indiquer votre nom comme Webmestre (affiché tout en bas de la page).
	2. Modifier la page 📄`index.html` afin d'afficher l'image 📄`port.jpg` à la place de l'image 📄`crieepoulgoazec.jpg`.
	3. Modifier la liste à puces suivante :
		<center>![](./images/port1.png)</center>
		afin de rajouter la ligne "financée à 85% par la région" et d'obtenir le résultat suivant :
		<center>![](./images/port2.png)</center>
	4. Dans la page 📄`visite.html`, modifier les horaires pour obtenir le résultat suivant :
		<center>![](./images/port3.png)</center>
	5. Sur les 3 pages HTML, ajouter le lien hypertexte qui permet d'ouvrir la page 📄`contacts.html` quand l'utilisateur clique sur l'option `Contacts` du menu.


## 2. Modification du CSS

On souhaite (entre autre) modifier l'apparence de tous les titres (c'est-à-dire les balises `h1` et `h2`), pour qu'ils soient centrés et en gris.

Pour cela, nous n'allons pas tous les modifier un par un dans toutes les pages HTML du site, mais nous alons créer une feuille de style, c'est-à-dire un fichier CSS, qui va nous permettre de modifier d'un seul coup tous les titres.

???+exercice "Exercice 2 - Activation du CSS"
	1. Dans les trois fichiers HTML, la ligne 7 est un commentaire. Un commentaire se paramètre ainsi :
		``` html
		<!--commentaire-->
		```
		Modifier cette ligne en supprimant les marques de commentaire afin de faire le lien entre les pages HTML et la feuille de style.
		La balise correspondante permet d'indiquer quelle feuille de style doit être utilisée.
	2. Enregistrer votre travail puis visualiser dans le navigateur les pages du site.
	3. Que constate-t-on lorsqu'on passe la souris sur un lien ?

???+exercice "Exercice 3 - Modification du CSS"
	Ouvrir la page 📄`style1.css` présente dans le dossier 📂`Styles` dans votre éditeur de texte.
	À chaque question, il n'y a q'une modification à effectuer dans ce fichier.

	4. Modifier la couleur de l'arrière plan (en rouge par exemple).
	5. Modifier les listes de puces afin d'obtenir le résultat suivant :
		<center>![](./images/port4.png)</center>
	6. Modifier le style de la bordure du bloc `section`.


## 3. Pour aller plus loin

???+exercice "Exercice 4 - Pour aller plus loin"
	La page d'accueil présente un lien vers la page 📄`visite.html` qui présente toutes les activités de la criée. Le responsable souhaite ajouter les contacts à la fin de la page afin de permettre aux internautes de téléphoner pour se renseigner sur les visites.

	1. Modifier la page 📄`visite.html`, et insérer le texte suivant au-dessus du tableau des horaires :
		<center>![](./images/port5.png)</center>
	2. Modifier la page 📄`visite.html`, et insérer le tableau suivant après le tableau des horaires.

		| Nom | Téléphone | Courriel |
		|---    |:-:    |:-:    |
		| M. Maréchal | 06.65.34.87.56 | marechal@poulgoazec.com |
		| M. Grumier  | 06.23.54.16.78 | grumier@poulgoazec.com |
		
	3. Tout comme cela a été fait pour les mots "description" et "horaires", faites-en sorte qu'un clic sur le mot "Contacts" conduise directement au tableau des contacts en bas de page.
	4. Dans la feuille de style, inversez les listes ordonnées pour obtenir d'abord des lettres et ensuite des nombres.
	5. Amusez-vous à modifier la présentation et la charte graphique selon vos goûts ! !
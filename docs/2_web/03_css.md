---
author: "Benoît Domart"
title: "Activité 3 - CSS"
---

Se connecter à l'ENT Netocentre, puis ouvrir l'activité Capytale suivante :

<center>
    [Activité 3 - CSS](https://capytale2.ac-paris.fr/web/c/0b14-4812989){ .md-button target=_blank}
</center>

💻 **Question 1** : Dans la zone réservée au CSS de l'activité sur Capytale, écrivez le code suivant et observez l'impact sur le style de la page.

```css
h1 {
    text-align: center;
    color: blue;
    font-size: 40px;
}

h2 {
    color: white;
    background-color: black;
}

p {
    font-family: Verdana;
}
```

✍️ **Question 2** : Indiquez alors le rôle de chacune des lignes de ce code CSS.

💻 **Question 3** : Modifiez le code CSS pour remplacer la police **Verdana** des paragraphes en la police **Arial**.

✍️💻 **Question 4** : Que faudrait-il ajouter comme code pour que les titres de niveau 3 (balises `<h3>`) soient centrés et de police **Arial** ? Faites-le et vérifiez en regardant les changements sur la page.

💻 **Question 5** : Ajoutez le code suivant au début de votre fichier CSS (inutile de recopier ce qui se trouve entre /* et */ qui sont des commentaires non pris en compte) et observez les changements.

```css
body {
    width: 800px;  /* largeur de 800 pixels */
    margin: auto;  /* marges automatiques pour centrer horizontalement */
    background-color: rgb(240, 255, 255);  /* couleur du fond */  
}
```
*N’hésitez pas à essayer de modifier les valeurs pour observer les changements !*

💻 **Question 6** : Ajoutez les règles suivantes pour le sélecteur h2 et observez les changements pour les titres de niveau 2 :

```css
padding-left: 20px;
padding-top: 5px;
padding-bottom: 5px;
border-radius: 10px;
```

**Explications** :

- L'unité de mesure `px` signifie "pixels" (`20px` signifie donc 20 pixels).
- `padding` désigne les marges intérieures d’un élément. Ici on a modifié les marges intérieures gauche (`padding-left`), haute (`padding-top`) et basse (`padding-bottom`) des balises `<h2>`.
- `border-radius` définit des coins arrondis pour la bordure d’un élément.

💻 **Question 7** : Ajoutez les règles suivantes pour le sélecteur `p` et observez les changements

```css
margin-left: 20px;
margin-right: 100px;
```

**Explications** : `margin` désigne les marges extérieures d'un élément. Ici on a modifié les marges gauche (`margin-left`) et droite (`margin-right`) des balises `<p>`.

💻 **Question 8** : Ajoutez les règles suivantes pour le sélecteur `p` et observez les changements. `td, th` signifie qu'on modifie **à la fois** les balises `td` et la balises `th`.

```css
table {
    border: 5px solid;
  }
  
td, th {
    border: 1px solid;
}
```
---
author: "Benoît Domart"
title: "Activité 7 - Les moteurs de recherche"
---

## Fonctionnement d'un moteur de recherche

???+abstract "Fonctionnement d'un moteur de recherche"

    Premières idées :
	
	- Faire appel à des experts.
	- Demander aux internautes de voter.
	
	Ces deux modèles, pourtant intéressants et logiques (ils sont mis en œuvre par Wikipédia), font intervenir l'humain et trouvent leur limite dans le trop grand nombre de pages a gérer qui sont de plus évolutives. Ce qui conduit a privilégier des protocoles entièrement automatisables sous forme d'algorithme.

	L'idée à la base du modèle de Larry Page et Sergey Brin, fondateurs de Google, revient à attribuer à chaque page un nombre positif entre 0 et 1, appelé score (en anglais *PageRank* de la page, qui caractérisera la pertinence de cette page. Pour cela, ils utilisent un **surfeur aléatoire**.
	
	**Principe du surfeur aléatoire :**
	
	Après avoir fait la liste (sans classement) de tous les sites traitant la requête, le surfeur aléatoire en choisit au hasard un. Nous avons vu que les sites webs sont reliés entre eux par des liens hypertextes. Ces liens sont donc orientés (un lien de la page web n°1 vers la page web n°2 n'est pas la même chose qu'un lien de la page web n°2 vers la page web n°1) :
	
	<center>
		![](./images/schema.PNG)
	</center>

	Mathématiquement, on peut représenter cette situation par ce qu'on appelle un **graphe**. Les **sommets** représentent les différentes pages web et les **arcs orientés** représentent les liens hypertextes :
	<center>
		![](./images/exercice1.PNG)<br/>
		Graphe n°1
	</center>
	

	Pour savoir dans quel ordre les sites doivent être affichés, l'algorithme de *PageRank* fonctionne de la façon suivante :
	
	1. On choisit un des sites au hasard.
	2. Ce site possède des liens hypertextes vers d'autres sites. On en choisit un au hasard.
	3. On répète cette opération sans s'arrêter en comptant pour chacun des sites combien de fois il l'a visité.
	
	Les sites sont alors affichés dans l'ordre décroissant de leur nombre de visites.

## Algorithme du pagerank avec Python

???+tip "Algorithme du pagerank avec Python"

	Le programme suivant implémente l'algorithme du *pagerank*.

	Lorsque vous souhaitez l'utiliser sur un graphe particulier, il suffit de mettre à jour les lignes surlignées en jaune ci-dessous, c'est-à-dire :

	1. La liste des sites, `liste_sites`, en en ajoutant ou en en enlevant en fonction du graphe considéré.

		`liste_sites = [1, 2, 3, 4]` signifie qu'il y a 4 sites dans le graphe, dont les noms sont 1, 2, 3 et 4.

		**L'ordre des sites n'est pas important**.
	2. En indiquant, pour chaque site, les liens hypertextes qu'il contient.
		
		Par exemple, `hypertexte[1]=[2,3]` signifie que le site n°1 contient un lien hypertexte vers le site n°2, et un lien hypertexte vers le site n°3.
		
		**À nouveau, l'ordre des sites n'est pas important**.
	3. Les `...` sont à enlever, ou a remplacer par une ligne semblable à ce qui précède s'il y a plus de sites dans le graphe considéré.

	```py hl_lines="4 6 7 8 9 10"
	import random

	####### Début de la partie à modifier #######
	liste_sites = [1, 2, 3, 4]
	hypertexte = {}
	hypertexte[1]=[2,3]
	hypertexte[2]=[3]
	hypertexte[3]=[1,2]
	hypertexte[4]=[3,2]
	...
	######## Fin de la partie à modifier ########


	nombre_passage = {}
	# On choisit la première page au hasard parmi toutes les pages.
	page = random.choice(liste_sites)
	nombre_passage[page] = 1
	# On se déplace 100 fois au hasard dans le graphe
	for _ in range(100):
		page = random.choice(hypertexte[page])
		if page in nombre_passage:
			nombre_passage[page] += 1
		else:
			nombre_passage[page] = 1

	print(nombre_passage)
	```

	!!!warning "Exécuter le programme plusieurs fois"

		Ce programme contient de l'aléatoire. Il ne renvoie donc pas toujours le même résultat. **Il est donc important, pour chaque graphe, de l'exécuter plusieurs fois**, et de regarder si l'ordre obtenu (c'est ça qui est important) est toujours le même !
	
	!!!success "Exécuter un programme Python"

		Pour exécuter un programme Python, sans avoir à l'installer sur son ordinateur, on peut aller sur [Basthon](https://console.basthon.fr/){target=_blank} !

## Exercice 1

???+exercice "Exercice 1"
	
	Appliquer cet algorithme au graphe n°1 ci-dessus. On complétera le tableau fourni sur la feuille.
	
	Dans quel ordre les pages doivent être affichées ?

## Exercice 2

???+exercice "Exercice 2"

	Appliquer à nouveau l'algorithme du *PageRank* sur le graphe suivant :
	<center>
		![](./images/exercice2.PNG)<br/>
		Graphe n°2
	</center>
	Dans quel ordre les pages doivent-elles êtres affichées ?

## Exercice 3 - Le puits

???+exercice "Exercice 3 - Le puits"

	1. Calculer les *PageRank* du graphe suivant. Dans quel ordre les pages doivent-elles êtres affichées ?
	2. Comparer avec les autres groupes.
	3. Quel problème (moral) est soulevé par ce graphe ?

	<center>
		![](./images/exercice4.PNG)<br/>
		Graphe n°3
	</center>

## Exercice 4 - La poche

???+exercice "Exercice 4 - La poche"

	1. Calculer les *PageRank* du graphe suivant. Dans quel ordre les pages doivent-elles êtres affichées ?
	2. Comparer avec les autres groupes.
	3. Quel problème est soulevé par ce graphe ?

	<center>
		![](./images/exercice5.PNG)<br/>
		Graphe n°4
	</center>

## Exercice 5

???+exercice "Exercice 5"

	Proposer une (ou plusieurs) solution pour éviter les problèmes soulevés par les graphes des exercices 3 et 4.

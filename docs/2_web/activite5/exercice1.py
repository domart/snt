import random

hypertexte = {}
hypertexte['A']=['E']
hypertexte['B']=['A', 'E']
hypertexte['C']=['A', 'F']
hypertexte['D']=[...]
hypertexte['E']=...
hypertexte['F']=...


# On se déplace nb_repetitions fois au hasard dans le graphe
nb_repetitions = 100000

nombre_passage = {}
# On choisit la première page au hasard parmi toutes les pages.
page = random.choice(list(hypertexte.keys()))
nombre_passage[page] = 1
for _ in range(nb_repetitions):
    page = random.choice(hypertexte[page])
    if page in nombre_passage:
        nombre_passage[page] += 1
    else:
        nombre_passage[page] = 1

for site in nombre_passage:
    nombre_passage[site] *= 100/nb_repetitions
    nombre_passage[site] = round(nombre_passage[site])

print(dict(sorted(nombre_passage.items(), key=lambda item:item[0])))
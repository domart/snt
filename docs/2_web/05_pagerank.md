---
author: "Benoît Domart"
title: "Activité 5 - PageRank"
---

Se connecter à l'ENT Netocentre.

???+exercice "Exercice 1 - Retour sur l'activité 5 du cours"

    <center>
        [Activité 5 - PageRank](https://capytale2.ac-paris.fr/web/c/b4be-4816490){ .md-button target=_blank}
    </center>

    Le script proposé permet d'exécuter l'algorithme du **PageRank** et renvoie le pourcentages de visites sur chaque site. Dans ce script, il vous ai demandé de comprendre les lignes 4 à 9 (qui sont surlignées). Il faudra savoir les modifier dans l'exercice suivant.

    ```py hl_lines="4 5 6 7 8 9" title="PageRank"
    import random

    hypertexte = {}
    hypertexte['A']=['E'] # (1)!
    hypertexte['B']=['A', 'E'] # (2)!
    hypertexte['C']=['A', 'F'] # (3)!
    hypertexte['D']=[...] # (4)!
    hypertexte['E']=... # (5)!
    hypertexte['F']=... # (6)!


    # On se déplace nb_repetitions fois au hasard dans le graphe
    nb_repetitions = 100000

    nombre_passage = {}
    # On choisit la première page au hasard parmi toutes les pages.
    page = random.choice(list(hypertexte.keys()))
    nombre_passage[page] = 1
    for _ in range(nb_repetitions):
        page = random.choice(hypertexte[page])
        if page in nombre_passage:
            nombre_passage[page] += 1
        else:
            nombre_passage[page] = 1

    for site in nombre_passage:
        nombre_passage[site] *= 100/nb_repetitions
        nombre_passage[site] = round(nombre_passage[site])

    print(dict(sorted(nombre_passage.items(), key=lambda item:item[0])))
    ```

    1. Depuis le site "A", on ne peut aller que sur le site "E".
    1. Depuis le site "B", on peut aller sur le site "A" ou sur le site "E".
    1. Depuis le site "C", on peut aller sur le site "A" ou sur le site "F".
    1. À compléter. Où peut-on aller quand on est sur le site "D" ?
    1. À compléter. Où peut-on aller quand on est sur le site "E" ?
    1. À compléter. Où peut-on aller quand on est sur le site "F" ?
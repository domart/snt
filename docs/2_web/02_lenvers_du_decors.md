---
author: "Benoît Domart"
title: "Activité 2 - L'envers du décors"
---

Ouvrir le fichier 📄`exempleHtml.html` dans Capytale (il faut d'abord se connecter à l'ENT Netocentre) :

<center>
    [Activité 2 - L'envers du décors](https://capytale2.ac-paris.fr/web/c/a52f-4812867){ .md-button target=_blank}
</center>

???+exercice "Exercice 1"

    Dans votre livret, compléter le tableau suivant et décrire l'effets des différentes balises indiquées dans le tableau en précisant pour chacune d'elle si elle a une importance pour la structure de la page ou pour son apparence :

    |   Balise   |   Effet |   Structure ou apparence ? |
    |---    |---    |:-:    |
    | `<title>` |  | |
    | `<h1>` |  | |
    |  | Titre moins important | |
    |  | Paragraphe | |
    |  | Image | |
    | `<s>` |  | |
    | `<ol>` |  | |
    | `<a>` |  | |
    | `<li>` |  | |
    |  | Définir une liste non ordonnée | |

???+ exercice "Exercice 2"

    Dans Capytale, modifier le fichier `exempleHtml.html` pour obtenir le rendu ci-dessous.

    Vous devez donc :

    1. Augmenter la taille de l'image de la licorne.
    1. Rendre cliquable le texte "**civilisation de l'Indus**", en redirigeant vers la page Wikipedia correspondante.
    1. Modifier le texte **Les licornes existent vraiment !!** pour qu'il ne soit plus barré, mais qu'il soit en gras !
    1. Ajouter l'image du Rhinocéros (elle est disponible sur la page Wikipedia correspondante).
    1. Ajouter l'image du Narval (elle est disponible sur la page Wikipedia correspondante).
    1. Ajouter le 3ème point de la liste en bas : **3. du merveilleux**.

    <center>
        ![](./activite2/objectif.png)
    </center>
---
author: "Benoît Domart"
title: "Activité 5 - Casser le code"
source: http://algoprog.fr/05-divers/nsi/defis_web/defi_web_1/defi_web_1.html
---

???+exercice "Exercice 1 - Casser le code"

    Va sur [cette page](./index.html){target=_blank}, et trouve le code secret !

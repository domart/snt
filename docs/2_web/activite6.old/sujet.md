---
author: "Benoît Domart"
title: "Activité 6 - Les cookies"
---

???+exercice "Exercice 1 - Les cookies"

    <center>
		![type:video](./videos/Comment%20effacer%20ses%20cookies.mp4)
	</center>

    Après avoir regardé la vidéo **Comment effacer ses cookies** ci-dessus, répondre aux questions suivantes :
	
    1. Expliquer ce qu'est un cookie.
	2. Donner trois exemples de cookies.
	3. D'après vous, faut-il supprimer les cookies présents sur son ordinateur ? Argumenter votre réponse.

	Puis chercher comment paramétrer et supprimer les cookies avec votre navigateur web.

---
author: "Benoît Domart"
title: "TP Noté"
---

Se connecter à l'ENT Netocentre, puis ouvrir l'activité Capytale suivante.

<center>
    [TP Noté](https://capytale2.ac-paris.fr/web/c/5a27-4937414){ .md-button target=_blank}
</center>

**Le TP sera à rendre de cette façon, via Capytale.**


???+exercice "Grille d'évaluation pour un projet de site web (avec évaluation collective)"

    Le code HTML sera notamment vérifié de manière automatique avec [le service de validation de W3C](https://validator.w3.org/#validate_by_input){target=_blank}.

    **Ce barème est fourni à titre indicatif et il pourra évoluer.**

    1. **Organisation et structure du site (3 points)**

        - [ ] Le site contient plusieurs pages (au moins 3) reliées entre elles par des liens de navigation (2 points)
        - [ ] Une structure HTML correcte (balises `#!html <html>`, `#!html <head>`, `#!html <body>`, etc.) est respectée sur chaque page (1 point)

    2. **Contenu HTML (9 points)**

        - [ ] Utilisation correcte et variée des balises HTML (titres `#!html <h1>` à `#!html <h3>` au moins, paragraphes `#!html <p>`, listes numérotées `#!html <ol>` et non numérotées `#!html <ul>`, images `#!html <img>`, vidéos `#!html <video>`, tableaux `#!html <table>`, liens hypertextes `#!html <a>`, etc.) (7 points)
        - [ ] Texte structuré et clair (titres, sous-titres, contenu lisible) (2 points)

    3. **Apparence et style avec CSS (7 points)**

        - [ ] Le fichier CSS est correctement lié au fichier HTML (1 point)
        - [ ] Utilisation d'un style cohérent pour l'ensemble du site (couleurs, polices, marges, etc.) (2 points)
        - [ ] Utilisation d'au moins 3 propriétés CSS différentes (par exemple : `#!css color`, `#!css font-size`, `#!css padding`, etc.) sur plusieurs balises (4 points)

    5. **Créativité et effort individuel (3 points)**

        - [ ] L'élève a montré une certaine créativité dans la conception du site (choix des couleurs, des images, du contenu) (1 point)
        - [ ] Effort global et respect des consignes (2 points)

    6. **Collaboration et travail d'équipe (3 points)**

        - [ ] Répartition équitable des tâches (chacun a contribué de manière visible) (2 points)
        - [ ] Bonne communication et entraide entre les membres du groupe (1 point)
---
author: Romain Janvier
title: Le Titanic
---

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />


# Activité sur les données du Titanic


## Utilisation d'un tableur

??? abstract "Le fichier `titanic.csv`"
    Pour ce TP, nous allons utiliser le fichier `titanic.csv` qui contient des informations sur une partie des passagers du Titanic.

    ![Image du Titanic](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/RMS_Titanic_3.jpg/800px-RMS_Titanic_3.jpg){ width=50% .center }

    Le Titanic est un paquebot qui a coulé en 1912 lors de son voyage inaugural. Il y avait environ 1500 personnes sur le bateau.

    Pour faire cette partie du TP, il faut :
    
    1.  Télécharger le fichier
        <center>
            [`titanic.csv`](./titanic.csv){ .md-button target=_blank }
        </center>
    2.  Faire un clic droit sur le fichier, sélectionner `ouvrir avec` puis `LibreOffice`. Vous pouvez aussi ouvrir `LibreOffice Calc` et ouvrir le fichier.

        ![ouvrir avec LibreOffice](images/ouvrir-avec.png){ .center }
        
    3.  Dans le menu d'import, il faut faire un clic droit sur les colonnes `Age` et `Fare` et sélectionner `Anglais US`. Il faut également décocher `Espace` comme séparateur s'il est coché.

        ![Mettre les nombres sur Anglais US](images/anglais-us.png){ .center }
        
    Vous pouvez maintenant explorer les données.

??? abstract "Traduction des descripteurs"
    Les descripteurs du fichier sont en anglais. Voici leur signification :

    - `Survived` : est-ce que le passager a survécu (0 pour non et 1 pour oui)
    - `Pclass` : classe dans laquel il voyageait (1ère, 2ème ou 3ème classe)
    - `Name` : nom complet du passager
    - `Sex` : genre du passager (`male` pour homme et `female` pour femme)
    - `Age` : âge du passager
    - `Siblings/Spouses Aboard` : nombre de frêres/sœurs ou époux/épouse à bord
    - `Parents/Children Aboard` : nombre de parents ou enfants à bord
    - `Fare` : prix du billet, en livres sterling

???exercice "Exercice 1"
    ![Rechercher](images/rechercher.png){ align=right }

    Pour faire les recherches, vous pouvez utiliser `Édition` ➔ `Rechercher` (raccourci ++ctrl+"F"++) ou la barre de recherche en bas à droite si elle est affichée.

    ![Rechercher](images/rechercher_b.png)

    1. Ce fichier est une table représentant une collection d'objets. Quels sont les "objets" ici ?
    1. Combien y a-t-il de descripteurs ?
    1. D'après ce fichier, combien y avait-il de personnes à bord ?
    2. **Miss. Constance Mirium West** a-t-elle survécu ?
    3. Dans quelle classe voyageait-elle ?
    4. Comment s’appelaient ses parents ?
    5. À eux trois, combien le voyage leur a-t-ils coûté ?
    


??? abstract "Filtrer les données"
    Pour pouvoir faire des recherches plus précises, il faut filtrer les données. Pour cela il faut appuyer sur le bouton `AutoFiltre` 

    ![AutoFiltre](images/autofiltre.png){ .center }    

    En cliquant sur la flèche à côté de chaque descripteur, vous pouvez sélectionner les valeurs que vous voulez afficher ou non.

    ![Rechercher](images/filtrer.png){ .center }
    
    Dans la barre en bas de la fenêtre, vous pouvez voir le nombre d'enregistrements restants après avoir filtré les données. 

    ![AutoFiltre](images/nb-resultats.png){ .center }    

    Si cela n'apparaît pas, il faut cliquer sur une cellule pour qu'elle s'affiche.


???exercice "Exercice 2"
    1. Combien de personnes ont survécu ?
    2. Combien de femmes ont survécu ?
    2. Combien de femmes de 40 ans ont survécu ?
    1. Quel est le pourcentage de passage de première classe à avoir survécu ?
    1. Quel est le pourcentage de passage de seconde classe à avoir survécu ?
    1. Quel est le pourcentage de passage de troisième classe à avoir survécu ?

    ???+tip "Rappel - calcul de pourcentage"

        Pour calculer le premier pourcentage, il faut calculer

        \[\dfrac{\text{nombre de passages de première ayant survécu}}{\text{nombre de passagers de première en tout}}\]

???abstract "Trier les données"

    Dans un fichier CSV, il est possible de trier les données, selon un descripteur, soit dans l'ordre croissant, soit dans l'ordre décroissant.

    Pour cela, dans le menu, il faut aller dans `Données` > `Trier...`.

    On peut alors choisir le descripteur et préciser si l'ordre du tri.

???exercice "Exercice 3"

    1. Quel était l'age (en mois) du plus jeune passager ?
    1. Quel était l'age du passager le plus agé ?
    1. Quel était le prix du billet le plus cher ?
    1. Combien de passagers ont payé ce prix ?

??? abstract "Calculer le taux de survie"
    Afin de calculer le taux de survie, rentrer dans la cellule `J1` l'expression :

    ``` title=""
    =SOUS.TOTAL(9;A2:A888)/SOUS.TOTAL(3;A2:A888)
    ```

    Cette formule compte le nombre de survivants et le divise par le nombre de personnes restant après application des filtres. Vous pouvez appuyer sur le bouton `Pourcentage` pour mettre le résultat en pourcentage.

    Cette formule utilise deux fois la fonction `SOUS.TOTAL`, dont vous pouvez trouver l'aide ici : [https://wiki.documentfoundation.org/FR/Calc:_fonction_SOUS.TOTAL](https://wiki.documentfoundation.org/FR/Calc:_fonction_SOUS.TOTAL){target=_blank}.

    1. `SOUS.TOTAL(9;A2:A888)`. Cette fonction a deux paramètres :
    
        - `9` : On indique qu'on va faire la somme des données concernées.
        - `A2:A888` : On précise la plage de valeurs (c'est-à-dire la liste des cellules) sur lesquels on va faire le calcul. Ici, on fait le calcul sur les données de la colonne `A`, de la cellule `A2` à la cellule `A888`.

        Ici, on fait donc la somme de toutes les données indiquant si la personne a survécu (valeur `1`) ou non (valeur `0`). On a donc le nombre de personne ayant survécu au naufrage.
    
    2. `SOUS.TOTAL(3;A2:A888)`. Cette fonction a deux paramètres :
    
        - `3` : On indique qu'on veut calculer le nombre de valeurs présentes dans la plage concernées.
        - `A2:A888` : On précise la plage de valeurs (c'est-à-dire la liste des cellules) sur lesquels on va faire le calcul. Ici, on fait le calcul sur les données de la colonne `A`, de la cellule `A2` à la cellule `A888`.

        Ici, on détermine donc le nombre de personnes totales.
    
    En faisant la division, on détermine donc le taux de survie.


    ![Bouton pourcentage](images/taux-survie-pourcentage.png){ .center }    

???exercice "Exercice 4"
    
    1. Quel est le taux de survie ?
    2. Quelle formule faut-il écrire pour calculer la somme des prix de tous les billets ?
    3. Quelle formule faut-il calculer pour déterminer l'age moyen des passagers ?



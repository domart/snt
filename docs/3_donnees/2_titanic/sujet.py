# --- hdr, 1 --- #
import matplotlib
import matplotlib.pyplot as plt

# Précision du backend à utiliser
matplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")

# Insertion de la courbe dans une div spécicfiqué (id="cible")
from js import document
document.pyodideMplTarget = document.getElementById("cible_1")
# On vide la div
document.getElementById("cible_1").textContent = ""

from js import fetch

url_fichier = "titanic.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()
lignes = contenu.strip().splitlines()

donnees = []

descript = lignes[0].split(",")

for ligne in lignes[1:]:
    row = {descript[i]: ligne.split(",")[i] for i in range(len(descript))}
    for clef in ['Survived', 'Pclass', 'Siblings/Spouses Aboard', 'Parents/Children Aboard']:
        row[clef] = int(row[clef])
    for clef in ['Age', 'Fare']:
        row[clef] = float(row[clef])
    donnees.append(row)


liste_clefs = list(donnees[0].keys())

valeurs_par_clef = {clef : set() for clef in liste_clefs}

clefs_discretes = ["Pclass", "Sex"]

clefs_continues = {"Age" : 5, "Fare" : 20}

conversion_clefs = {"age" : "Age", "tarif" : "Fare", "classe" : "Pclass", "sexe" : "Sex", "survie" : "Survived", "nom" : "Name"}


for perso in donnees:
    for clef in liste_clefs:
        valeurs_par_clef[clef].add(perso[clef])

for clef in valeurs_par_clef:
    valeurs_par_clef[clef] = sorted(valeurs_par_clef[clef])

def egal_ou_dans(valeur, val_ens):
    return val_ens == None or valeur == val_ens or (isinstance(val_ens, list) and valeur in val_ens)

def regrouper_classes(clef, amplitude, classe=None, sexe=None, age_min=0, age_max=100, tarif_min=0, tarif_max=1000):
    resultats = {}
    for perso in donnees:
        val = perso[clef]
        val = int(val - val%amplitude)
        if egal_ou_dans(perso["Sex"],sexe) and \
           egal_ou_dans(perso["Pclass"],classe) and \
           age_min <= perso["Age"] <= age_max and \
           tarif_min <= perso["Fare"] <= tarif_max:
            if val not in resultats:
                resultats[val] = 0
            resultats[val] += 1
    #return sorted(resultats.items())
    return sorted(resultats.keys())

def remplir_tableau(clef1, clef2, classe=None, sexe=None, age_min=0, age_max=100, tarif_min=0, tarif_max=1000):
    if clef1 in clefs_discretes:
        vals1 = valeurs_par_clef[clef1]
    else:
        amplitude1 = clefs_continues[clef1]
        vals1 = regrouper_classes(clef1, amplitude1, classe, sexe, age_min, age_max, tarif_min, tarif_max)
    
    if clef2 in clefs_discretes:
        vals2 = valeurs_par_clef[clef2]
    else:
        amplitude2 = clefs_continues[clef2]
        vals2 = regrouper_classes(clef2, amplitude2, classe, sexe, age_min, age_max, tarif_min, tarif_max)
    table = [ [ [0, 0] for _ in range(len(vals1)+1)] for _ in range(len(vals2)+1)]
    #print(clef1,vals1)
    #print(clef2,vals2)
    for perso in donnees:
        if egal_ou_dans(perso["Sex"],sexe) and \
           egal_ou_dans(perso["Pclass"],classe) and \
           age_min <= perso["Age"] <= age_max and \
           tarif_min <= perso["Fare"] <= tarif_max:
            v1 = perso[clef1]
            v2 = perso[clef2]
            if clef1 in clefs_continues:
                v1 = int(v1 - v1%amplitude1)
            if clef2 in clefs_continues:
                v2 = int(v2 - v2%amplitude2)
            i = vals2.index(v2)
            j = vals1.index(v1)
            table[i][j][perso["Survived"]] += 1
    for i in range(len(vals2)):
        for j in range(len(vals1)):
            for k in range(2):
                table[i][len(vals1)][k] += table[i][j][k]
                table[len(vals2)][j][k] += table[i][j][k]
                table[len(vals2)][len(vals1)][k] += table[i][j][k]
    if clef1 in clefs_continues:
        for i, v in enumerate(vals1):
            vals1[i] = f"[{v}:{v+amplitude1}["
    if clef2 in clefs_continues:
        for i, v in enumerate(vals2):
            vals2[i] = f"[{v}:{v+amplitude2}["    
    return table, vals1+["Réunis"], vals2+["Réunis"]

def afficher_condition(clef, condition):
    if isinstance(condition, list):
        if len(condition)>1:
            val = "{" + " ; ".join([str(v) for v in condition]) + "}"
            return f"{clef} \u2208 {val}"
        elif len(condition)==1:
            return f"{clef} = {condition[0]}"
        else:
            return ""
    else:
        return f"{clef} = {condition}"

def afficher_tableau(clef2, clef1, classe=None, sexe=None, age_min=0, age_max=100, tarif_min=0, tarif_max=1000):
    plt.clf()
    if clef1.lower() not in conversion_clefs:
        print("Descripteur",clef1,"inconnu")
    elif clef2.lower() not in conversion_clefs:
        print("Descripteur",clef2,"inconnu")
    else:
        c1 = clef1
        c2 = clef2
        clef1 = conversion_clefs[clef1.lower()]
        clef2 = conversion_clefs[clef2.lower()]
        table, vals1, vals2 = remplir_tableau(clef1, clef2, classe, sexe, age_min, age_max, tarif_min, tarif_max)
        table_couleurs = [ [(1,1,1)]*len(table[0]) for _ in range(len(table))]
        for i in range(len(table)):
            for j in range(len(table[i])):
                m, v = table[i][j]
                if m+v>0:
                    #table[i][j] = f"V : {v:} | M : {m:} | {100*v/(max(m+v,1)):5.2f}%"
                    taux = v/max(m+v,1)
                    table[i][j] = f"{m+v:4} ; {100*taux:6.2f}%"
                    table_couleurs[i][j] = (1-taux,taux,0, 0.2)
                else:
                    table[i][j] = ""
        the_table = plt.table(cellText=table,
                          rowLabels=vals2,
                          colLabels=vals1, loc='center',
                              cellColours = table_couleurs)
        the_table.auto_set_font_size(False)
        the_table.set_fontsize(10)
        the_table.scale(1,1.2)
        plt.axis('off')
        titre = f"{c2}\{c1} : Personnes ; Taux de survie\n"
        complements = []
        if classe:
            res = afficher_condition("classe", classe)
            if res:
                complements.append(res)
        if sexe:
            res = afficher_condition("sexe", sexe)
            if res:
                complements.append(res)
        complements.append(f"{age_min} \u2264 age \u2264 {age_max}")
        complements.append(f"{tarif_min} \u2264 tarif \u2264 {tarif_max}")
        titre += " et ".join(complements)
        plt.title(titre)
        plt.show()

def afficher_personnes(tri = [], classe=None, sexe=None, age_min=0, age_max=100, tarif_min=0, tarif_max=1000):
    liste = []
    nb_survivants = 0
    for perso in donnees:
        if egal_ou_dans(perso["Sex"],sexe) and \
           egal_ou_dans(perso["Pclass"],classe) and \
           age_min <= perso["Age"] <= age_max and \
           tarif_min <= perso["Fare"] <= tarif_max:
            liste.append(perso)
            if perso['Survived']:
                nb_survivants += 1
    """ Survie | sexe | age | classe |  tarif | nom """
    if tri:
        tri = [conversion_clefs[c.lower()] for c in tri]
        fonction = lambda v : tuple((v[clef] for clef in tri))
        liste.sort(key = fonction)
    print(" Nb | Surv. | Sexe |  Age | Classe |  Tarif | Nom")
    for i, perso in enumerate(liste):
        age = perso['Age']
        if age.is_integer():
            age = int(age)
        print(f"{i+1:3} |  {['non','oui'][perso['Survived']]}  |   {perso['Sex'][0].upper()}  | {age:4} |    {perso['Pclass']}   | {perso['Fare']:6.2f} | {perso['Name']}")
    if liste:
        print(f"Taux de survie : {100*nb_survivants/len(liste):.2f}%")

# --- exo, 1 --- #
# Vous pouvez utiliser le terminal à l'aide des fonctions
# afficher_tableau(DESCR1, DESCR2)
# afficher_personnes()
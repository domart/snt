---
author: Romain Janvier
title: 7. Les fonctions
---

# Les fonctions en Python

???+ abstract "Définir une fonction"
    Lorsqu’on sait qu’on va utiliser plusieurs fois une figure, on peut créer une nouvelle commande qui permet de dessiner cette figure en une seule ligne.

    Les **fonctions** Python servent à cela. On peut voir une fonction comme une nouvelle *commande* ajoutée à celles déjà disponibles.

    Pour définir une fonction, il faut utiliser la syntaxe :
    ```python
    def nom_de_la_fonction(parametres):  # (1)!
        instruction_1  # (2)!
        instruction_2
        ...
        instruction_n
    ```

    1.   Sur la première ligne :
        -  Le **mot-clef `#!python def`** au début de la ligne.
        -  Le **nom de la fonction**, avec les mêmes règles syntaxiques que les noms de variables.
        -  Les **parenthèses** qui peuvent être vides ou contenir les noms de paramètres, séparés par des virgules s'il y en a plusieurs.
        -  Les **deux points** à la fin de la ligne
    2.   Sur les lignes suivantes, les instructions ont toutes la même **indentation**, c'est-à-dire le décalage vers la droite par rapport à la première ligne. C'est ce qui permet à Python de déterminer à quel moment s'arrête la fonction. Si l'indentation n'est pas correcte, vous aurez un message d'erreur.
         
         Le code de la fonction s'arrête dès qu'il y a une ligne sans indentation.

???+ note "Un exemple de fonction"
    Dans le programme ci-dessous, il y a le code de la fonction `pic` qui permet de tracer une figure.

    {{ IDE('scripts/exo_7_exemple_1', TERM_H=6) }}

    {{ figure('cible_3', div_class="py_mk_figure admonition", admo_kind="") }}
    
    ??? warning "Si vous ne voyez pas la figure"
        Si vous exécutez le programme tel qu'il est, il ne va rien se passer. 

        C'est parce que vous définissez la fonction, mais que vous ne l'appelez pas.

        Vous pouvez tester cette fonction de deux façons différentes :

        === "Dans le terminal"
            Vous pouvez rentrer les commandes suivantes (ou d'autres) pour tester la fonction :
            
            ```pycon
            >>> pic()
            >>> left(90)
            >>> pic()
            ```

        === "Dans l'éditeur"
            Vous pouvez rajouter les lignes suivantes (ou d'autres) après la fonction, sans indentation :
            
            ```python
            pic()
            left(90)
            pic()
            ```

            Il faut appuyer sur le bouton "Exécuter" pour voir le résultat.

???+ note "Utilisation dans une autre fonction"
    À partir de maintenant, la fonction `pic` est pré-définie pour les exemples et exercices suivants.

    Cette fonction peut être utilisée pour écrire une autre fonction.

    {{ IDE('scripts/exo_7_exemple_2', TERM_H=6) }}

    {{ figure('cible_4', div_class="py_mk_figure admonition", admo_kind="") }}

???+ question "Exercice 7 (recopier la réponse sur la feuille)"

    ![](images/figures-tortue_3_sombre.svg#only-dark){ align=right }
    ![](images/figures-tortue_3.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python etoile()` afin d'obtenir la figure ci-contre. Il faut faire attention à l'indentation et appuyer sur le bouton `Exécuter` après toute modification du fichier pour qu'elle soit prise en compte.

    Le triangle rouge indique la position de départ et d'arrivée et les triangles bleus indiquent les positions intermédiaires.

    ??? tip "Indications"
        ??? tip "Indication 1"
            Vous pouvez comparer la figure attendu avec celle de l'exemple précédent.

        ??? tip "Indication 2"
            Il faut utiliser 4 fois `pic` et tourner 4 fois, comme dans l'exemple précédent.

        ??? tip "Indication 3"
            Si on ne veut pas faire la croix, il ne faut pas tourner dans le même sens.
            

    :warning: Vous devez utiliser la fonction `pic` qui est pré-définie. Il n'est pas nécessaire de rajouter le code de cette fonction.
    
    Recopiez le code de la fonction `etoile` sur la feuille.

    {{ IDE('scripts/exo_7', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_5', div_class="py_mk_figure admonition", admo_kind="") }}



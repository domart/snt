/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
//defaultpen(fontsize(12));
//defaultpen(linewidth(1));
settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);
void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

//sombre();


void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

void polyreg(pair pos, real angle, real l, int n,int sens=1,pen col=currentpen)
{
  pair p1=pos;
  pair p2=pos+l*dir(angle);
  for (int i=0;i<n;++i)
  {
   p2=p1+l*dir(angle);
   draw(p1--p2,col);
   angle+=sens*(360.0/n);
   p1=p2;
  }
}

/* couche 1 */
// suite carrés
real n=5;
real l=1.5;
pair pos=(0,0);
tortue(pos,red);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l,4);//,i*(1.0/(1.2*n))*white);
   pos+=(l,0);
}
tortue(pos,blue);

/* couche 2 */
// suite carrés espaces
real n=5;
real l=1.2;
pair pos=(0,0);
tortue(pos,red);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l,4);//,i*(1.0/(1.2*n))*white);
   pos+=(1.5l,0);
}
tortue(pos,blue);


/* couche 3 */
// rosace de carres
real n=6;
for (int i=0;i<n;++i)
   polyreg((0,0),i*360.0/n,1,4);//,i*(1.0/(1.2*n))*white);

/* couche 4 */
// rosace de carres
real n=12;
for (int i=0;i<n;++i)
   polyreg((0,0),i*360.0/n,1.5,4);//,i*(1.0/(1.2*n))*white);
tortue((0,0),red);
 


/* couche 5 */
// rosace d'hexagones
for (int i=0;i<6;++i)
   polyreg((0,0),i*60,.9*1.5,6);
tortue((0,0),red);

/* couche 6 */
// rosace d'hexagones 2
int n=24;
for (int i=0;i<n;++i)
   polyreg((0,0),i*360.0/n,.9*1.5,6);
tortue((0,0),red);

/* couche 7 */
// cercle de carres
real l=.6*2;
real n=6;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,-i*360.0/n,l,4);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*360.0/n);
   tortue(pos,-(i+1)*360.0/n,blue);

}
tortue((0,0),red);

/* couche 8 */
// cercle de pentagones
real l=.6*2;
real n=5;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,-i*360.0/n,l,5);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*360.0/n);
   tortue(pos,-(i+1)*360.0/n,blue);

}
tortue((0,0),red);

/* couche 9 */
// cercle d'hexagones
real l=.5*2;
real n=6;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,-i*360.0/n,l,6);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*360.0/n);
   tortue(pos,-(i+1)*360.0/n,blue);

}
tortue((0,0),red);

/* couche 10 */
// polygones avec le nb de cote qui augmente
for (int i=3;i<11;++i)
   polyreg((0,0),0,.8,i);

/* couche 11 */
// suite polygones
real l=.8;
real n=8;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l,3+i);//,i*(1.0/(1.2*n))*white);
   pos+=(1.5l,0);
   tortue(pos,0,blue);   
}
tortue((0,0),red);

/* couche 12 */
// carres imbriques
real l=2.5;
real n=10;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l*(1-.1*i),4);//,i*(1.0/(1.2*n))*white);
   //pos+=(1.5l,0);
}
tortue((0,0),red);

/* couche 13 */
// pentagones imbriques
real l=1.8;
real n=10;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l*(1-.1*i),5);//,i*(1.0/(1.2*n))*white);
   //pos+=(1.5l,0);
}
tortue((0,0),red);

/* couche 14 */
// carres imbriques moitie
real l=2.5;
real n=5;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l,4);//,i*(1.0/(1.2*n))*white);
   //pos+=(1.5l,0);
   l=l/2;
}
tortue((0,0),red);

/* couche 15 */
// carreaux imbriques
real l=2.5/2;
real n=4;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,0,l,4);//,i*(1.0/(1.2*n))*white);
   polyreg(pos+(l,0),0,l,4);
   polyreg(pos+(0,l),0,l,4);
   polyreg(pos+(l,l),0,l,4);
   //pos+=(1.5l,0);
   l=l/2;
}
tortue((0,0),red);


/* couche 16 */
// cercle multi
real l=.6;
real n=6;
pair pos=(0,0);
for (int i=0;i<2*n;++i)
{
   polyreg(pos,-i*360.0/n,l,3+i);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*360.0/n);
   tortue(pos,-(i+1)*360.0/n,blue);   
}
tortue((0,0),red);

/* couche 17 */
// spirale carres 2
real l=.03;
real n=23;
pair pos=(0,0);
for (int i=0;i<n;++i)
{
   polyreg(pos,-i*60,l,4);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*60);
   l*=1.2;
   if (i>12)
       tortue(pos,-(i+1)*360.0/6,blue);   
}
tortue((0,0),red);

/* couche 18 */
// carre de carres
real r = 1;
polyreg((0,0),0,r,4);//,i*(1.0/(1.2*n))*white);
polyreg((r,0),0,r,4);//,i*(1.0/(1.2*n))*white);
polyreg((0,r),0,r,4);//,i*(1.0/(1.2*n))*white);
polyreg((r,r),0,r,4);//,i*(1.0/(1.2*n))*white);
tortue((0,0),red);
tortue((0,2*r),-90,blue);
tortue((2*r,2*r),180,blue);
tortue((2*r,0),-270,blue);



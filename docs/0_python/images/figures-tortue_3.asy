/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);
void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

//sombre();

void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

pair tracer(pair pos, pair[] depl, pen coul=currentpen, real r=.3)
{
  for (pair vect:depl)
  {
     draw(pos -- pos+r*vect, coul);
     pos = pos+r*vect;
  }
  return pos;
}

/* couche 3 */
pair[] chemin1 = {E, NE, SE, E};
pair[] chemin2 = {S, SE, SW, S};
pair[] chemin3 = {W, SW, NW, W};
pair[] chemin4 = {N, NW, NE, N};

pair pos = tracer((0,0), chemin1);
tortue((0,0),red);
tortue(pos,-90,blue);
pos = tracer(pos, chemin2);
tortue(pos,180,blue);
pos = tracer(pos, chemin3);
tortue(pos,90,blue);
pos = tracer(pos, chemin4);


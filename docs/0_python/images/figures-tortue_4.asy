/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);
void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

//sombre();

void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

pair tracer(pair pos, pair[] depl, pen coul=currentpen, real r=.3)
{
  for (pair vect:depl)
  {
     draw(pos -- pos+r*vect, coul);
     pos = pos+r*vect;
  }
  return pos;
}

/* couche 4 */
real r = 2;
draw(box((0,0), (r,r)));
draw(box((0,0), .5(r,r)));
draw(shift((r, 0))*box((0,0), .5(r,r)));
tortue((0,0),red);
tortue((1
r,0),blue);


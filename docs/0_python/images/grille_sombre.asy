/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
//defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);

void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

sombre();

void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

void grille(pair pos, real longueur, int nb_cases) {
    real l = longueur/nb_cases;
    for (int i=0;i<=nb_cases;++i) {
        draw(shift(pos)*((0,i*l)--(longueur,i*l)));
        draw(shift(pos)*((i*l,0)--(i*l,longueur)));        
    }
}

grille((0,0), 5, 8);
tortue((0,0),0,blue);

/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
//defaultpen(fontsize(12));
//defaultpen(linewidth(1));
settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);
void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

sombre();


void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

void polyreg(pair pos, real angle, real l, int n,int sens=1,pen col=currentpen)
{
  pair p1=pos;
  pair p2=pos+l*dir(angle);
  for (int i=0;i<n;++i)
  {
   p2=p1+l*dir(angle);
   draw(p1--p2,col);
   angle+=sens*(360.0/n);
   p1=p2;
  }
}

/* couche 16 */
// cercle multi
real l=.6;
real n=6;
pair pos=(0,0);
for (int i=0;i<2*n;++i)
{
   polyreg(pos,-i*360.0/n,l,3+i);//,i*(1.0/(1.2*n))*white);
   pos+=l*dir(-i*360.0/n);
   tortue(pos,-(i+1)*360.0/n,blue);   
}
tortue((0,0),red);


/* debut */
import geometry;
import mesmacros;
//import operation_relatifs;
//import tableau_proport;
//import instruments_geometrie;
//import trembling;
unitsize(1cm);
defaultpen(fontsize(12));
//defaultpen(linewidth(1));
//settings.tex="pdflatex";
settings.outformat="svg";
//dotfactor=3;
//tremble tr=tremble();
//tremble tr=tremble(angle=10, frequency=0.5, random=50);
void sombre()
{
    currentpen = white;
    red = lightred;
    blue = lightblue;
}

//sombre();

void tortue(pair pos, real angle=0,pen coul)
{
  real r=0.3;
  path tt = (0,0)--(-r,r/2)--(-0.8*r,0)--(-r,-r/2)--cycle;
  fill(shift(pos)*rotate(angle)*tt,coul);
}

pair tracer(pair pos, pair[] depl, pen coul=currentpen, real r=.3)
{
  for (pair vect:depl)
  {
     draw(pos -- pos+r*vect, coul);
     pos = pos+r*vect;
  }
  return pos;
}

/* couche 1 */
real r = 1.5;
pair[] chemin1 = {E};
pair[] chemin2 = {dir(60)};
pair[] chemin3 = {dir(-20)};

pair pos1 = tracer((0, 0), chemin1, r);
pair pos2 = tracer(pos1, chemin2, r);
pair pos3 = tracer(pos2, chemin3, 2r);

tortue((0, 0), red);
tortue(pos1, 0, blue);
tortue(pos2, 60, blue);
tortue(pos3, -20, blue);

draw(pos1--pos1+.5r*dir(0), dotted+blue);
draw(pos2--pos2+.5r*dir(60), dotted+blue);
draw("$\ang{60}$", arc(pos1,.3r, 0, 60), blue, ArcArrow(TeXHead));
draw("$\ang{80}$", arc(pos2,.3r, -20, 60), blue, BeginArcArrow(TeXHead));

/* couche 2 */
real r = 1;
pair[] chemin1 = {E, S, E, N};
pair pos1 = tracer((0, 0), chemin1, r);
tortue((0, 0), red);
tortue(pos1, 90, blue);

/* couche 3 */
pair[] chemin1 = {E, NE, SE, E};
pair[] chemin2 = {S, SE, SW, S};
pair[] chemin3 = {W, SW, NW, W};
pair[] chemin4 = {N, NW, NE, N};

pair pos = tracer((0,0), chemin1);
tortue((0,0),red);
tortue(pos,-90,blue);
pos = tracer(pos, chemin2);
tortue(pos,180,blue);
pos = tracer(pos, chemin3);
tortue(pos,90,blue);
pos = tracer(pos, chemin4);

/* couche 4 */
real r = 2;
draw(box((0,0), (r,r)));
draw(box((0,0), .5(r,r)));
draw(shift((r, 0))*box((0,0), .5(r,r)));
tortue((0,0),red);
tortue((1
r,0),blue);


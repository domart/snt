---
author: Benoît Domart
title: 2. py-rates
---


???+ exercice "Exercice 1 - premiers programmes"
    Nous allons maintenant écrire nos premiers programmes : [https://py-rates.fr/](https://py-rates.fr/){target=_blank}.

    <red>Attention à bien noter votre identifiant ! En le prenant en photo avec ton téléphone par exemple !</red>
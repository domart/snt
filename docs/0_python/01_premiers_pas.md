---
author: Benoît Domart
title: 1. Premiers pas avec Python 
---


???+ info "Python ?"
    **Python** est un langage de programmation créé par Guido von Rossum en 1991. Le développement du langage est maintenant supervisé par la [Python Software Foundation](https://www.python.org/psf-landing/).
    
    **Python** est un langage **interprété**. Cela veut dire que les *instructions* sont lues et exécutées les unes après les autres par **l'interpréteur**.

    **Python** n'a rien à voir avec le serpent, Guido van Rossum est un fan d'un groupe de comiques anglais : les *Monty Python*. C'est en son honneur qu'il a baptisé son langage de programmation.

???+exercice "Exercice 1 - Premiers pas"

    Nous n'allons pas commencer par écrire des programmes en Python, mais nous allons commencer par nous mettre à la place de l'ordinateur, et exécuter des programmes écrits dans un langage qui ressemble beaucoup à Python. Pour cela, il suffit de ce rendre [ici](https://compute-it.toxicode.fr/?progression=python){target=_blank}.


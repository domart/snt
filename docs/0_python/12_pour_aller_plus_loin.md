---
author: Romain Janvier
title: 12. Pour aller plus loin
---

# Pour aller plus loin

???+ question "Exercice 18 (facultatif)"

    === "figure 1"
        ![](images/polygones-reguliers_10_sombre.svg#only-dark)
        ![](images/polygones-reguliers_10.svg#only-light)
    === "figure 2"
        ![](images/polygones-reguliers_11_sombre.svg#only-dark)
        ![](images/polygones-reguliers_11.svg#only-light)
    === "figure 3"
        ![](images/polygones-reguliers_16_sombre.svg#only-dark)
        ![](images/polygones-reguliers_16.svg#only-light)
    === "figure 4"
        ![](images/polygones-reguliers_17_sombre.svg#only-dark)
        ![](images/polygones-reguliers_17.svg#only-light)

        La taille augmente de 20% à chaque carré.

    Rajoutez des fonctions avec le nom et les paramètres de votre choix permettant d’obtenir les figures ci-dessus. 

    Pour les 3 premières figures, le nombre de côtés des polygones augmente de 1 à chaque étape.

    {{ IDE('scripts/exo_18', TERM_H=6) }}

    {{ figure('cible_19', div_class="py_mk_figure admonition", admo_kind="") }}

???+ question "Exercice 19 (facultatif)"

    Écrire le code de la fonction `grille` qui prend en paramètres un nombre `longueur` et un entier `nb_cases` et qui trace la grille carrée dont chaque côté mesure `longueur` pixels et a `nb_cases` cases.

    ![](images/grille_sombre.svg#only-dark){ .center }
    ![](images/grille.svg#only-light){ .center }


    {{ IDE('scripts/exo_19', TERM_H=6) }}

    {{ figure('cible_20', div_class="py_mk_figure admonition", admo_kind="") }}

???+ note "La documentation du module Tortue"
    Vous pouvez trouver plus d'informations sur la tortue en Python vous pouvez aller consulter [la documentation](https://docs.python.org/fr/3.10/library/turtle.html). Vous trouverez, par exemple, les commandes permettant de changer la couleur et l'épaisseur du trait.

from turtle import *
setup(640, 480)

def rosace2(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        right(360/nb_poly)

rosace2(4, 50, 6)

done()

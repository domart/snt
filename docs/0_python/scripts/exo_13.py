# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

_cible = 'cible_14'

# --------- PYODIDE:code --------- #
def rosace2(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        right(360/nb_poly)

rosace2(4, 50, 6)

# --------- PYODIDE:corr --------- #
def rosace2(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        forward(longueur)
        right(360/nb_poly)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


from turtle import *

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

def suite_de_carre(nb_carres, longueur):
    for i in range(nb_carres):
        carre(longueur)
        up()
        forward(2*longueur)
        down()

suite_de_carre(5, 25)

done()
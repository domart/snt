# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

_cible = 'cible_15'

# --------- PYODIDE:code --------- #
def carres_imbriques(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = ...

carres_imbriques(20)

# --------- PYODIDE:corr --------- #
def carres_imbriques(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = longueur + 10

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


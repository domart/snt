from turtle import *
setup(640, 480)

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

# Peut-être qu'il faut aller chercher une fonction
# d'un exercice précédent...

def carres_imbriques3(n):
    longueur = 10
    for i in range(n):
        ...  # peut-être qu'il faut utiliser autre chose que carre(longueur)...
        longueur = ...

carres_imbriques3(5)

done()

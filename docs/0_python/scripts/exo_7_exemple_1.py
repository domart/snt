# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

_cible = 'cible_3'

# --------- PYODIDE:code --------- #
from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

def pic():
    forward(50)
    left(45)
    forward(50)
    right(90)
    forward(50)
    left(45)
    forward(50)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    if len(str(Screen().canvas)) < 50:
        print("Il n'y a pas de figure ? C'est normal.")
        print("Vous devriez aller voir la remarque ci-dessous.")
    forward(0)
m_a_j(_cible)

# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


from turtle import *
setup(640, 480)

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

def suite_carres2(longueur, nb_carres):
    for i in range(nb_carres):
        carre(longueur)
        up()  # lever le stylo
        forward(...)
        down()  # baisser le stylo

suite_carres2(50, 5)

done()

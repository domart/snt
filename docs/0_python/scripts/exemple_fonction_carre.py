from turtle import *
setup(640, 480)

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

carre(50)
carre(100)

done()


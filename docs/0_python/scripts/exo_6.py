# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

_cible = 'cible_2'

# --------- PYODIDE:code --------- #
from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

# Mettre les instructions ci-dessous

# --------- PYODIDE:corr --------- #
from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

forward(80)
right(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


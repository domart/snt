from turtle import *
setup(640, 480)

def rosace(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        right(360/nb_poly)

# Mettre les paramètres correspondant
# à la figure que vous voulez voir
rosace(4, 100, 12)

done()

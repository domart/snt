# --- exo, ide --- #
print("Bonjour !")
print("4 * 5 =", 4*5)

# --- exo, print --- #
print("Ce texte va être affiché dans le terminal")
"Celui-ci ne sera pas affiché"
print(1 + 2)
1 + 3

# --- exo, commentaires --- #
# Cette ligne n'est pas lu par l'interpréteur
print("Cette ligne si")
# print("Mais pas celle-ci")

print("On peut même mettre") # des commentaires en bout de ligne

# --- exo, 5 --- #
a = 9
b = 2
a = a + b
b = a * b

# --- env, 6_exemple, 6, 7_exemple_1, 7_exemple_2, 7, 8_exemple, 8, 9_exemple, 9, 10, 11, 12, 13_exemple, 13, 14, 15, 16, 17, 18, 19 --- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

# --- post, 6_exemple, 6, 7_exemple_2, 7, 8_exemple, 8, 9_exemple, 9, 10, 11, 12, 13_exemple, 13, 14, 15, 16, 17, 18, 19 --- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --- post_term, 6_exemple, 6, 7_exemple_1, 7_exemple_2, 7, 8_exemple, 8, 9_exemple, 9, 10, 11, 12, 13_exemple, 13, 14, 15, 16, 17, 18, 19 --- #
if "m_a_j" in globals():
    m_a_j(_cible)

# --- entete, 6_exemple, 6, 7_exemple_1 | env, 7_exemple_2, 7, 8_exemple, 8, 9_exemple, 9, 10, 11, 12, 13_exemple, 13, 14, 15, 16, 17, 18, 19 --- #
from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

# --- env, 6_exemple --- #
_cible = 'cible_1'

# --- exo, 6_exemple --- #
forward(50)  # avancer de 50 pixels
left(60)     # tourner à gauche de 60 degrés
forward(50)  # avancer de 50 pixels
right(80)    # tourner à droite de 80 degrés
forward(100) # avancer de 100 pixels

# --- env, 6 --- #
_cible = 'cible_2'

# --- exo, 6 --- #
# Mettre les instructions ci-dessous

# --- corr, 6 --- #
forward(80)
right(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)

# --- env, 7_exemple_1 --- #
_cible = 'cible_3'

# --- exo, 7_exemple_1 | env, 7_exemple_2, 7, 8 --- #
def pic():
    forward(50)
    left(45)
    forward(50)
    right(90)
    forward(50)
    left(45)
    forward(50)

# --- post, 7_exemple_1 --- #
if Screen().html is None:
    if len(str(Screen().canvas)) < 50:
        print("Il n'y a pas de figure ? C'est normal.")
        print("Vous devriez aller voir la remarque ci-dessous.")
    forward(0)
m_a_j(_cible)

# --- env, 7_exemple_2 --- #
_cible = 'cible_4'

# --- exo, 7_exemple_2 --- #
def croix():
    pic()
    left(90)
    pic()
    left(90)
    pic()
    left(90)
    pic()
    left(90)

croix()

# --- env, 7 --- #
_cible = 'cible_5'

# --- exo, 7 --- #
def etoile():
    ...  # Mettre les instructions de la fonction ici

etoile()

# --- corr, 7 --- #
def etoile():
    pic()
    right(90)
    pic()
    right(90)
    pic()
    right(90)
    pic()
    right(90)

# --- env, 8_exemple --- #
_cible = 'cible_6'

# --- exo, 8_exemple --- #
def triangle():
    for i in range(3):
        forward(150)
        left(120)

triangle()

# --- env, 8 --- #
_cible = 'cible_7'

# --- exo, 8 --- #
def croix_v2():
    for i in range(...):
        ...
        ...

croix_v2()

# --- corr, 8 --- #
def croix_v2():
    for i in range(4):
        pic()
        left(90)

# --- env, 9_exemple --- #
_cible = 'cible_8'

# --- exo, 9_exemple | env, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19  --- #
def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

# --- exo, 9_exemple --- #
carre(50)
carre(100)

# --- env, 9 --- #
_cible = 'cible_9'

# --- exo, 9 --- #
carre(100)
carre(...)
forward(...)
carre(...)

# --- corr, 9 --- #
carre(100)
carre(50)
forward(100)
carre(50)

# --- env, 10 --- #
_cible = 'cible_10'

# --- exo, 10 --- #
def suite_carres(longueur, nb):
    for i in range(...):
        carre(...)
        forward(...)

suite_carres(50, 5)
suite_carres(20, 3)

# --- corr, 10 --- #
def suite_carres(longueur, nb):
    for i in range(nb):
        carre(longueur)
        forward(longueur)

# --- env, 11 --- #
_cible = 'cible_11'

# --- exo, 11 --- #
def suite_carres2(longueur, nb):
    for i in range(nb):
        carre(longueur)
        up()  # lever le stylo
        forward(...)
        down()  # baisser le stylo

suite_carres2(50, 3)
suite_carres2(20, 4)

# --- corr, 11 --- #
def suite_carres2(longueur, nb):
    for i in range(nb):
        carre(longueur)
        up()  # lever le stylo
        forward(1.5*longueur)
        down()  # baisser le stylo

# --- env, 12 --- #
_cible = 'cible_12'

# --- exo, 12 --- #
def carre_de_carres(longueur):
    for i in range(4):
        carre(longueur)
        ...
        ...

carre_de_carres(100)

# --- corr, 12 --- #
def carre_de_carres(longueur):
    for i in range(4):
        carre(longueur)
        forward(2*longueur)
        left(90)

# --- env, 16, 17, 18, 19 --- #
def carre_de_carres(longueur):
    for i in range(4):
        carre(longueur)
        forward(2*longueur)
        left(90)

# --- env, 13_exemple --- #
_cible = 'cible_13'

# --- exo, 13_exemple --- #
def rosace(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        right(360/nb_poly)

# Mettre les paramètres correspondant
# à la figure que vous voulez voir
rosace(4, 100, 12)

# --- env, 13 --- #
_cible = 'cible_14'

# --- exo, 13 --- #
def rosace2(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        right(360/nb_poly)

rosace2(4, 50, 6)

# --- corr, 13 --- #
def rosace2(nb_cotes, longueur, nb_poly):
    for i in range(nb_poly):
        for j in range(nb_cotes):
            forward(longueur)
            left(360/nb_cotes)
        forward(longueur)
        right(360/nb_poly)

# --- env, 14 --- #
_cible = 'cible_15'

# --- exo, 14 --- #
def carres_imbriques(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = ...

carres_imbriques(20)

# --- corr, 14 --- #
def carres_imbriques(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = longueur + 10

# --- env, 15 --- #
_cible = 'cible_16'

# --- exo, 15 --- #
def carres_imbriques2(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = ...

carres_imbriques2(5)

# --- corr, 15 --- #
def carres_imbriques2(n):
    longueur = 10
    for i in range(n):
        carre(longueur)
        longueur = 2*longueur

# --- env, 16 --- #
_cible = 'cible_17'

# --- exo, 16 --- #
def carres_imbriques3(n):
    longueur = 10
    for i in range(n):
        ...  # il faut utiliser autre chose que carre(longueur)...
        longueur = ...

carres_imbriques3(4)

# --- corr, 16 --- #
def carres_imbriques3(n):
    longueur = 10
    for i in range(n):
        carre_de_carres(longueur)  # il faut utiliser autre chose que carre(longueur)...
        longueur = longueur * 2

# --- env, 17 --- #
_cible = 'cible_18'

# --- exo, 17 --- #
def spirale(n):
    ...

spirale(10)

# --- corr, 17 --- #
def spirale(n):
    longueur = 5
    for i in range(n):
        forward(longueur)
        left(90)
        longueur = longueur + 5

# --- env, 18 --- #
_cible = 'cible_19'

# --- exo, 18 --- #
# Mettre vos fonctions ici

# --- env, 19 --- #
_cible = 'cible_20'

# --- exo, 19 --- #
def grille(longueur, nb_cases):
    ...

grille(100, 5)


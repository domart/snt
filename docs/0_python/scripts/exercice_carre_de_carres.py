from turtle import *
setup(640, 480)

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

def carre_de_carres(longueur):
    for i in range(4):
        carre(longueur)
        ...
        ...

carre_de_carres(100)

done()

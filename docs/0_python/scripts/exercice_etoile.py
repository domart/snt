from turtle import * # pour utiliser la tortue
setup(640, 480) # taille de la fenetre

def pic():
    forward(50)
    left(45)
    forward(50)
    right(90)
    forward(50)
    left(45)
    forward(50)

def etoile():
    ...  # Mettre les instructions de la fonction ici

etoile()

done()

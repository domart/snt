# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

_cible = 'cible_10'

# --------- PYODIDE:code --------- #
def suite_carres(longueur, nb):
    for i in range(...):
        carre(...)
        forward(...)

suite_carres(50, 5)
suite_carres(20, 3)

# --------- PYODIDE:corr --------- #
def suite_carres(longueur, nb):
    for i in range(nb):
        carre(longueur)
        forward(longueur)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


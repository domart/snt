# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

def carre(longueur):
    for i in range(4):
        forward(longueur)
        left(90)

_cible = 'cible_11'

# --------- PYODIDE:code --------- #
def suite_carres2(longueur, nb):
    for i in range(nb):
        carre(longueur)
        up()  # lever le stylo
        forward(...)
        down()  # baisser le stylo

suite_carres2(50, 3)
suite_carres2(20, 4)

# --------- PYODIDE:corr --------- #
def suite_carres2(longueur, nb):
    for i in range(nb):
        carre(longueur)
        up()  # lever le stylo
        forward(1.5*longueur)
        down()  # baisser le stylo

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)


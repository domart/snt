from turtle import * # pour utiliser la tortue
setup(640, 480) # taille de la fenetre

forward(50)  # avancer de 50 pixels
left(60)     # tourner à gauche de 60 degrés
forward(50)  # avancer de 50 pixels
right(80)    # tourner à droite de 80 degrés
forward(100) # avancer de 100 pixels

done()

---
author: Romain Janvier
title: 10. Les rosaces
---

# Les rosaces

???+ note "Des boucles dans des boucles"
    Il est possible de mettre des boucles dans des boucles. On dit que ce sont des **boucles imbriquées**.

    C'est le cas dans la fonction `rosace` suivante :


    {{ IDE('scripts/exo_13_exemple', TERM_H=6) }}

    {{ figure('cible_13', div_class="py_mk_figure admonition", admo_kind="") }}


    Cette fonction dessine `nb_poly` polygones réguliers de `nb_cotes` côtés, mesurant chacun `longueur` pixels par côté.

    Voici quelques exemples :

    | 12 carrés | 6 hexagones | 24 hexagones |
    |:---------:|:-----------:|:------------:|
    |`#!python rosace(4, 100, 12)`|`#!python rosace(6, 100, 6)`|`#!python rosace(6, 100, 24)`|
    |![](images/polygones-reguliers_4_sombre.svg#only-dark)![](images/polygones-reguliers_4.svg#only-light)|![](images/polygones-reguliers_5_sombre.svg#only-dark)![](images/polygones-reguliers_5.svg#only-light)|![](images/polygones-reguliers_6_sombre.svg#only-dark)![](images/polygones-reguliers_6.svg#only-light)|

    La tortue fait un tour sur elle-même à chaque polygone et un tour au total lorsqu’elle trace tous les polygones. Un tour complet correspond à 360°.

???+ abstract "Pour accélérer le tracé de la figure"
    Afin de permettre à la tortue d'aller plus vite, vous pouvez rajouter `speed(10)` au début de votre script ou avant l'appel à la fonction qui trace l'image.

    La valeur du paramètre de `speed` peut aller de 1 à 10 pour aller de plus en plus vite. Si on met 11 ou 0, le tracé se fait quasi instantanément.

???+ question "Exercice 13 (répondre sur la feuille)"
    
    Modifiez le code de la fonction `#!python rosace2` qui permet de faire les figures ci-dessous.

    | 6 carrés | 5 pentagones | 6 hexagones |
    |:---------:|:-----------:|:------------:|
    |`#!python rosace2(4, 50, 6)`|`#!python rosace2(5, 50, 5)`|`#!python rosace2(6, 50, 6)`|
    |![](images/polygones-reguliers_7_sombre.svg#only-dark)![](images/polygones-reguliers_7.svg#only-light)|![](images/polygones-reguliers_8_sombre.svg#only-dark)![](images/polygones-reguliers_8.svg#only-light)|![](images/polygones-reguliers_9_sombre.svg#only-dark)![](images/polygones-reguliers_9.svg#only-light)|

    Pour l'instant le code de `rosace2` est identique à celui de `rosace`.

    ??? tip "Indication"
        Il suffit de rajouter une ligne contenant `#!python forward(longueur)`.

    {{ IDE('scripts/exo_13', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_14', div_class="py_mk_figure admonition", admo_kind="") }}



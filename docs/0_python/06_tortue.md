---
author: Romain Janvier
title: 6. Utilisation de la tortue
---

???+ abstract "La tortue"
    La **tortue** est un module de Python qui permet de tracer des figures, comme avec Scratch.

    Pour utiliser la tortue, il faut ajouter les lignes suivantes au début du fichier Python :

    ```python title="Lignes à mettre au début du fichier"
    from turtle import *  # pour utiliser la tortue
    setup(640, 480)  # pour définir la taille de l'image
    ```

    :warning: Ces lignes seront mises dans les premiers exemples pour permettre un copier/coller dans un autre IDE, mais elles seront chargées automatiquement pour la suite des exemples et exercices.

    La tortue est représentée par l'image d'une tortue ou un petit triangle. Pour la contrôler, vous pouvez utiliser les commandes suivants :

    | Commande Python | Remarques|
    |:---------------:|:---------|
    |`#!python forward(N)` ou `#!python fd(N)` | Avancer de `#!python N` pixels|
    |`#!python backward(N)` ou `#!python back(N)` ou `#!python bk(N)` | Reculer de `#!python N` pixels|
    |`#!python right(N)` ou `#!python rt(N)` | Tourner à droite de `#!python N` degrés|
    |`#!python left(N)` ou `#!python lt(N)` | Tourner à gauche de `#!python N` degrés|
    |`#!python up()` | Lever le crayon|
    |`#!python down()` | Baisser le crayon|
    |`#!python reset()` | Tout réinitialiser|

???+ info "Utilisation de la tortue sur le site"
    Pour les programmes liés à la tortue sur le site, voici quelques remarques :

    * Vous pouvez écrire le code dans la partie **éditeur**.

    * Pour exécuter le script et obtenir la figure, il faut appuyer sur le bouton `Exécuter`. 

    * À chaque modification du fichier, il faut appuyer à nouveau sur `Exécuter`.

    * Après avoir exécuté votre programme, vous pouvez utiliser les
      commandes de la tortue dans le **terminal** pour continuer la
      figure.

    * La ligne `#!py setup(640, 480)` n'a pas d'effet sur le site. Elle est juste là au cas où vous préférer copier le code dans un autre éditeur, comme `Thonny` qui est installé sur le site.

    ??? warning "Si rien ne s'affiche"
        Si la figure ne s'affiche pas, il faut regarder dans le terminal et voir s'il n'y a pas un message d'erreur. 

        En cas d'erreur, le programme s'arrête avant d'afficher la figure.

???+ note "Un premier exemple" 

    ![](images/figures-tortue_1_sombre.svg#only-dark){ align=right }
    ![](images/figures-tortue_1.svg#only-light){  align=right }

    Les instructions suivantes permettent d'obtenir la figure ci-contre :

    ```python
    forward(50)  # avancer de 50 pixels
    left(60)     # tourner à gauche de 60 degrés
    forward(50)  # avancer de 50 pixels
    right(80)    # tourner à droite de 80 degrés
    forward(100) # avancer de 100 pixels
    ```

    Le triangle rouge correspond à la position de départ et celle en bleu à l'arrivée.

    Vous pouvez tester ces instructions ci-dessous. Si vous appuyez une deuxième fois sur le bouton "Exécuter", vous pouvez voir la tortue dessiner la figure.

    {{ IDE('scripts/exo_6_exemple', TERM_H=7) }}

    {{ figure('cible_1', div_class="py_mk_figure admonition", admo_kind="") }}

???+ question "Exercice 6 (répondre sur la feuille)"

    ![](images/figures-tortue_2_sombre.svg#only-dark){ align=right }
    ![](images/figures-tortue_2.svg#only-light){ align=right }
    
    Trouver l'ensemble d'instructions à utiliser pour obtenir la figure ci-contre. 

    Chaque trait mesure 80 pixels de long et les angles font tous 90°.

    Une fois la figure obtenue, il faut recopier les instructions que
    vous avez utilisées sur votre feuille.

    ??? tip "Indications"
        ??? tip "Indication 1"
            Vous pouvez commencer par recopier les instructions permettant de faire la figure de l'exemple et les modifier ensuite.

        ??? tip "Indication 2"
            Il faut avancer 4 fois et tourner 3 fois.

        ??? tip "Indication 3"
            Le code doit ressembler à :

            ```python
            forward(...)
            ...  # On tourne
            forward(...)
            ...  # On tourne
            forward(...)
            ...  # On tourne
            forward(...)
            ```

    {{ IDE('scripts/exo_6', TERM_H=7, MODE="revealed") }}    

    {{ figure('cible_2', div_class="py_mk_figure admonition", admo_kind="") }}

  

---
author: Romain Janvier
title: 9. Fonctions et paramètres
---

# Fonctions et paramètres

???+ info "Rajouter des paramètres aux fonctions"
    Les figures obtenues par les fonctions précédentes ont toujours la même taille. Si on veut changer la taille du triangle obtenu avec `triangle`, il faut changer la longueur du côté avant d'exécuter à nouveau le code.

    Si on veut tracer 3 triangles de tailles différentes sur la même figure, ce n'est pas possible avec cette version de la fonction.

    Par contre certaines fonctions prennent des paramètres, comme les fonctions `left`, `right` ou `forward`. 

    Selon la valeur mise dans les parenthèses, le résultat sera différent.


???+ note "Ajouter un paramètre à une fonction"
    Prenons l'exemple de la fonction suivante :

    ```python
    def carre():
        for i in range(4):
            forward(50)
            left(90)
    ```

    Tous les carrés tracés feront alors 50 pixels de long.

    Nous pouvons rajouter un paramètre pour permettre de décider de la taille du carré lors de l'appel de la fonction :

    ```python
    def carre(longueur):
        for i in range(4):
            forward(longueur)
            left(90)
    ```

    Si on appelle `#!python carre(50)` on aura un carré de 50 pixels de côté. Ce sera 100 pixels si on appelle `#!python carre(100)`.

    ??? note "Vous pouvez tester ici"

        {{ IDE('scripts/exo_9_exemple', TERM_H=6) }}

        {{ figure('cible_8', div_class="py_mk_figure admonition", admo_kind="") }}

    Voici ce qui se passe quand on fait ces deux appels :

    === "Les deux appels à la fonction"
        ```python
        carre(50)
        carre(100)
        ```
    === "On remplace par le code de la fonction"
        ```python
        for i in range(4):  # premier carré : carre(50)
            forward(50)  # longueur est remplacé par 50
            left(90)
        for i in range(4):  # deuxième carré : carre(100)
            forward(100)  # longueur est remplacé par 100
            left(90)
        ```
    === "On déplie la boucle"
        ```python
        forward(50)  # premier tour de boucle du premier carré
        left(90)
        forward(50)  # deuxième tour de boucle du premier carré
        left(90)
        forward(50)  # troisième tour de boucle du premier carré
        left(90)
        forward(50)  # quatrième tour de boucle du premier carré
        left(90)
        forward(100)  # premier tour de boucle du deuxième carré
        left(90)
        forward(100)  # deuxième tour de boucle du deuxième carré
        left(90)
        forward(100)  # troisième tour de boucle du deuxième carré
        left(90)
        forward(100)  # quatrième tour de boucle du deuxième carré
        left(90)
        ```

???+ question "Exercice 9 (recopier la réponse sur la feuille)"

    ![](images/figures-tortue_4_sombre.svg#only-dark){ align=right }
    ![](images/figures-tortue_4.svg#only-light){ align=right }
    
    Complétez cette suite d’instructions pour obtenir la figure ci-dessous. Les petits carrés mesurent 50 pixels de côté.

    {{ IDE('scripts/exo_9', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_9', div_class="py_mk_figure admonition", admo_kind="") }}

???+ abstract "Plusieurs paramètres"
    Il est possible d’utiliser plusieurs paramètres pour une fonction. Il faut alors séparer les paramètres dans la définition, ou les valeurs lors de l’appel, par des virgules.

???+ question "Exercice 10 (recopier la réponse sur la feuille)"

    ![](images/polygones-reguliers_1_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_1.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python suite_carres` qui prend en paramètres `longueur` et `nb`, et qui trace `nb` carrés, tous de longueur `longueur` pixels. Par exemple, l'appel de `#!python suite_carres(50, 5)` permet d'obtenir les 5 carrés ci-contre. 

    Le code de votre fonction doit permettre d'afficher `#!python suite_carres(50, 5)` et `#!python suite_carres(20, 3)` à la suite. Vous obtiendrez donc 5 grands carrés suivis de 3 petits carrés.

    :warning: Vous ne devez pas modifier la ligne `#!python def suite_carres(longueur, nb):`.

    ??? tip "Indications"
        ??? tip "Indication 1"
            Vous ne devez mettre aucun nombre dans le code de la fonction, uniquement les variables `longueur` et `nb`.
            
            Il faut utiliser chacune des variables au moins une fois.

        ??? tip "Indication 2"
            Vous pouvez compléter le code avec les valeurs permettant d'obtenir la bonne figure pour `#!python suite_carres(50, 5)` puis par celles correspondant à `#!python suite_carres(20, 3)`.

            Vous pouvez ensuire regarder quelles valleurs correspondent à `longueur` et à `nb`.

        ??? tip "Indication 3"
            Pour chacun des trous à remplir, vous pouvez vous demander s'il faut mettre une longueur ou le nombre de fois qu'on doit répéter les instructions.

    {{ IDE('scripts/exo_10', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_10', div_class="py_mk_figure admonition", admo_kind="") }}


???+ question "Exercice 11 (répondre sur la feuille)"

    ![](images/polygones-reguliers_2_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_2.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python suite_carres2` qui prend en paramètres `longueur` et `nb`, et qui trace `nb` carrés, tous de longueur `longueur` pixels. L'écart enere deux carrés mesure la moitié de `longueur`. La figure ci-contre correspond à `#!python suite_carres2(50, 5)`.

    Il est possible de mettre une expression contenant des calculs dans les parenthèses, comme `3 * longueur` ou `longueur + 0.2*longueur`. 

    Recopiez la valeur mise dans les parenthèses de `forward` sur la feuille.

    ??? tip "Indication"
        La distance qui sépare le coin gauche d'un carré de celui du suivant correspond à une fois et demie la longueur du côté de chaque carré.

    
    {{ IDE('scripts/exo_11', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_11', div_class="py_mk_figure admonition", admo_kind="") }}



???+ question "Exercice 12 (recopier la réponse sur la feuille)"

    ![](images/polygones-reguliers_18_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_18.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python carre_de_carres` qui prend en paramètres `longueur` et qui trace un carré composé de 4 carrés, tous de longueur `longueur`.
    La tortue rouge indique la position de départ et d'arrivée et les tortues bleues indiquent les positions intermédiaires.

    ??? tip "Indication"
        Pour tracer le grand carré, il faut dessiner les 4 petits carrés. Les points de départ des petits carrés correspondent aux coins du grand carré.

    {{ IDE('scripts/exo_12', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_12', div_class="py_mk_figure admonition", admo_kind="") }}



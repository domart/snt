---
author: Romain Janvier
title: 11. Tailles variables
---

# Tailles variables

???+ question "Exercice 14 (répondre sur la feuille)"

    ![](images/polygones-reguliers_12_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_12.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python carres_imbriques` qui prend en paramètres un entier `n` et qui permet d'obtenir la figure ci-contre, avec `n` carrés. Le plus petit carré mesure 10 pixels de côté et chaque carré mesure 10 pixels de plus que les autres.
 
    Recopiez le code de la fonction sur la feuille.

    ??? tip "Indications"
        ??? tip "Indication 1"
            On rappelle qu'on peut calculer la prochaine valeur d'une variable à partir de son ancienne valeur.

            ```pycon
            >>> a = 5
            >>> a = a + 2
            >>> a
            7
            >>> a = a + 2
            >>> a
            9
            ```

        ??? tip "Indication 2"
            La variable `longueur` prend successivement les valeurs 10, 20, 30, 40...

            Comment calculer la prochaine valeur de cette variable ?

    {{ IDE('scripts/exo_14', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_15', div_class="py_mk_figure admonition", admo_kind="") }}

    
    
???+ question "Exercice 15 (répondre sur la feuille)"

    ![](images/polygones-reguliers_14_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_14.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python carres_imbriques2` qui prend en paramètres un entier `n` et qui permet d'obtenir la figure ci-contre, avec `n` carrés. Le plus petit carré mesure 10 pixels de côté et chaque carré est 2 fois plus grand que le précédent.
 
    Recopiez le code de la fonction sur la feuille.

    ??? tip "Indication"
        Cette fois la variable prend pour valeur 10, 20, 40, 80...

    {{ IDE('scripts/exo_15', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_16', div_class="py_mk_figure admonition", admo_kind="") }}

        
???+ question "Exercice 16 (répondre sur la feuille)"

    ![](images/polygones-reguliers_15_sombre.svg#only-dark){ align=right }
    ![](images/polygones-reguliers_15.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python carres_imbriques3` qui prend en paramètres un entier `n` et qui permet d'obtenir la figure ci-contre, avec `n` carrés. Le plus petit carré mesure 10 pixels de côté et la taille double à chaque étape.
 
    Recopiez le code de la fonction sur la feuille.

    ??? tip "Indication"
        Il faut utiliser une fonction que vous avez écrite dans un exercice précédent. Cette fonction est déjà en mémoire, il n'est pas nécessaire de remettre son code.

    {{ IDE('scripts/exo_16', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_17', div_class="py_mk_figure admonition", admo_kind="") }}

       
???+ question "Exercice 17 (répondre sur la feuille)"

    ![](images/python-spirale-carree_sombre.svg#only-dark){ align=right }
    ![](images/python-spirale-carree.svg#only-light){ align=right }
    
    Complétez le code de la fonction `#!python spirale` qui prend en paramètres un entier `n` et qui permet d'obtenir la spirale ci-contre, avec `n` segments au total. Le premier segment mesure 5 pixels et chaque segment est 5 pixels plus long que le précédent.
 
    Recopiez le code de la fonction sur la feuille.

    !!! tip "Pas d'indications pour cet exercice"
    
    {{ IDE('scripts/exo_17', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_18', div_class="py_mk_figure admonition", admo_kind="") }}

    

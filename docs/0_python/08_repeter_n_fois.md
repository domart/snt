---
author: Romain Janvier
title: 8. Répéter n fois
---

# Répéter $n$ fois

???+ info "Les boucles"
    On remarque qu’on est amené à répéter plusieurs fois la même suite d’instructions. Afin d’éviter cela, on utilise des **boucles**.

    La fonction `#!python croix()` utilise 4 fois les mêmes instructions. Cela peut se simplifier avec une boucle pour qui correspond, dans ce cas, à "*répéter 4 fois*".

???+ abstract "Répéter $n$ fois"
    La structure générale d'une boucle de type "*répéter $n$ fois*" est :
    ```python
    for i in range(n): # (1)!
        instruction_1 # (2)!
        instruction_2
        ...
        instruction_k
    ```

    1. Les mots clef `#!python for` et `#!python in` sont obligatoires, tout comme les "`:`" à la fin. Le nom de la **variable de boucle**, ici `i`, peut être n'importe quel autre nom de variable. En pratique, on utilise souvent `i`, `j` ou `k`.
    2. Comme pour les fonctions, l'indentation permet de déterminer les instructions qui font partie de la boucle et celles qui n'y sont pas.

    Cela revient à répéter $n$ fois la suite d'instructions. 

???+ note "Un exemple de boucle"
    On considère la fonction suivante :

    ```python
    def triangle():
        for i in range(3):
            forward(150)
            left(120)
    ```

    ??? note "Vous pouvez tester la fonction ici"

        {{ IDE('scripts/exo_8_exemple', TERM_H=6) }}

        {{ figure('cible_6', div_class="py_mk_figure admonition", admo_kind="") }}

    Lors de l'appel à la fonction, le code est transformé successivement pour arriver aux instructions de base :

    === "L'appel à la fonction"
        ```python
        triangle()
        ```
    === "On remplace par le code de la fonction"
        ```python
        for i in range(3):
            forward(150)
            left(120)
        ```
    === "On déplie la boucle"
        ```python
        forward(150)  # Premier tour de boucle
        left(120)
        forward(150)  # Deuxième tour de boucle
        left(120)
        forward(150)  # Troisième tour de boucle
        left(120)
        ```

???+ question "Exercice 8 (recopier la réponse sur la feuille)"
    
    Complétez le code de la fonction `#!python croix_v2` afin d'obtenir la même figure qu'avec la fonction `croix` vu précédemment. 

    :warning: Vous devez utiliser la fonction `pic` qui est pré-définie.

    ??? info "Pour rappel, le code de la fonction `croix`"
        ```python
        def croix():
            pic()
            left(90)
            pic()
            left(90)
            pic()
            left(90)
            pic()
            left(90)
        ```

    ??? tip "Indication"
        Il y a 2 lignes qui sont répétées plusieurs fois dans le code de `croix`.

        Il faut juste rajouter ces deux lignes et le nombre de fois qu'il faut répéter ces lignes.
    {{ IDE('scripts/exo_8', TERM_H=6, MODE="revealed") }}

    {{ figure('cible_7', div_class="py_mk_figure admonition", admo_kind="") }}
 

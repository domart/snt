---
author: Benoît Domart
title: 4. La console 
---


???+ info "Python ?"
    **Python** est un langage de programmation créé par Guido von Rossum en 1991. Le développement du langage est maintenant supervisé par la [Python Software Foundation](https://www.python.org/psf-landing/).
    
    **Python** est un langage **interprété**. Cela veut dire que les *instructions* sont lues et exécutées les unes après les autres par **l'interpréteur**.

    **Python** n'a rien à voir avec le serpent, Guido van Rossum est un fan d'un groupe de comiques anglais : les *Monty Python*. C'est en son honneur qu'il a baptisé son langage de programmation.

???+ info "L'interpréteur"

    L'interpréteur peux exécuter un grand nombre d'instructions différentes, mais les plus simples sont les expressions mathématiques. Dans ce cas, les expressions sont évaluées et le résultat affiché :
    
    ```pycon
    >>> 2 + 3 * 5
    17
    ```
    
    Les 3 chevrons `>>>` correspondent à des lignes où l'interpréteur attend une instruction à exécuter ou une expression à évaluer. 

    Ainsi, lorsqu'on voit `>>> 2 + 3 * 5`, cela veut dire que l'utilisateur a tapé `2 + 3 * 5`. 
    
    Les lignes qui ne commencent pas par `>>>` correspondent aux _réponses_ de l'interpréteur. 

    Ainsi, après avoir évalué l'expression `2 + 3 * 5`, il renvoit le résultat du calcul qui est `17`. 

    On remarquera qu'il respecte les priorités de calcul. On peut donc utiliser Python comme une calculatrice. Mais Python ne fait pas que des calculs.
    
    ```pycon
    >>> "Bonjour"
    'Bonjour'
    ```
    
    Les expressions ne correspondent pas toujours à des calculs à proprement parler. Dans l'exemple précédent, L'expression est un *texte*. 

    Les textes, en Python, doivent être mis entre guillemets (`#!python "un texte"`) ou appostrophes (`#!python 'un autre texte'`). Lorsque l'interpréteur lit une expression qui correspond à un texte, il l'évalue et renvoie le texte qui correspond. Par contre, il n'essaie pas de comprendre ce que cela peut vouloir dire. Par conséquence, si le texte contient un calcul, celui-ci ne sera pas effectué :
    
    ```pycon
    >>> "2 + 3 * 5"
    '2 + 3 * 5'
    ```

???+ info "Le terminal"
    On accède à l'interpréteur dans un **terminal** (ou **console**), comme celui ci-dessous. Vous pouvez expérimenter un peu avec ce terminal. Si vous obtenez un message d'erreur, ce n'est pas grave.
    
    Vous pouvez revenir dans l'historique des instructions/expressions déjà tapées en utilisant la flèche du haut du clavier.
    
    Vous pouvez aussi effacer le contenu du terminal avec l'instruction `clear`. Par contre, cela n'efface pas l'historique.
    
    {{ terminal() }}

???+ exercice "Exercice 1"
    Pour chacune des expressions suivantes, déterminer la réponse qui sera affichée dans le terminal.
    
    === "QCM"
        {{multi_qcm(
            ["**Expression 1 :** `#!python 25 + 5`",
                [
                    " `#!python 30`",
                    " `#!python '25 + 5'`",
                    " `#!python '30'`",
                    " `#!python 25 + 5`"
                ],
                [1]
            ],
            ["**Expression 2 :** `#!python '25 + 5'`",
                [
                    " `#!python 30`",
                    " `#!python '25 + 5'`",
                    " `#!python '30'`",
                    " `#!python 25 + 5`"
                ],
                [2]
            ]
        ) }}
        
    === "Explications"
        **Expression 1 :** L'expression `#!python 25 + 5` est une opération mathématique. L'interpréteur va donc évaluer cette expression et afficher le résultat : `#!python 30`. Il n'y a pas de guillemets, parce que ce n'est pas un texte.
        
        **Expression 2 :** L'expression `#!python '25 + 5'` est un texte puisqu'il y a des guillemets. Elle est donc affichée telle quelle dans le terminal.

???+ info "L'IDE"
    Le terminal est bien pratique pour tester le fonctionnement de Python. Mais il l'est beaucoup moins pour écrire un programme. En effet, il n'est pas imaginable d'écrire un programme ligne par ligne tout au long de l'exécution. Il faut écrire le programme en amont et l'exécuter ensuite. 

    Pour écrire ce programme, on utilise un **IDE** (Integrated Development Environment), ou *environnement intégré de développement*. Il existe de nombreux IDE pour Python. Certains doivent s'installer, comme [IDLE](https://www.python.org/downloads/){target=_blank} ou [Thonny](https://thonny.org/){target=_blank}. D'autres sont disponibles en ligne sans rien installer, comme [futurecoder](https://fr.futurecoder.io/course/#ide){target=_blank} ou [basthon](https://console.basthon.fr/){target=_blank}.
    
    {{ IDE("scripts/exemple_ide") }}

    <div class="py_mk_ide">Il suffit d'appuyer sur le bouton <button class="tooltip"><img src="/assets/images/icons8-play-64.png"></button> ci-dessus pour que le programme, dans la partie du haut, soit exécuté dans le terminal.</div>

    Une fois le programme exécuté, vous pouvez continuer à utiliser le terminal comme avant.

???+ info "La fonction `#!python print`"
    Dans l'exemple précédent vous pouvez remarquer que dans les deux lignes du programme, il y a la fonction `#!python print`. Cette fonction permet d'afficher des messages dans le terminal à l'exécution du programme. En effet, l'évaluation d'une expression dans le programme ne va pas forcément provoquer un affichage dans le terminal. Pour voir le résultat de l'évaluation d'une expression, il faut utiliser la fonction `#!python print`. Il suffit de mettre dans la parenthèse l'expression à afficher. On peut aussi afficher plusieurs expressions à la suite en les séparant par des virgules dans la fonction. Le résultat de chaque expression est alors séparé par un espace à l'affichage

    {{ IDE("scripts/exemple_print") }}

    Dans cet exemple on peut voir que les expressions qui ne sont pas dans un `#!python print` ne provoquent aucun affichage. On peut voir aussi que lorsqu'un texte est affiché avec cette fonction, il n'y a ni apostrophes, ni guillemets dans le terminal. C'est tout à fait normal.

    La fonction `#!python print` peut également être utilisée dans le terminal.

    ```pycon
    >>> "Avec les guillemets"
    'Avec les guillemets'
    >>> print("Sans les guillemets")
    Sans les guillemets
    >>> 2 + 3
    5
    >>> print(4 + 5)
    9
    ```

    Pour les textes, on voit bien la différence. Pour les expressions mathématiques, par contre, il n'y en a pas.

???+ exercice "Exercice 2"
    Écrire un programme qui affiche `Hello World!` dans le terminal. Traditionnellement, c'est le [premier programme](https://fr.wikipedia.org/wiki/Hello_world){target=_blank} que l'on écrit lorsqu'on apprend un nouveau langage de programmation.

    {{ IDE() }}

    ??? success "Solution"
        Il suffit d'écrire la ligne
        ```python
        print("Hello World!")
        ```

???+ info "Les commentaires"
    Lorsque les programmes deviendront plus long, il deviendra crucial de rajouter des **commentaires** pour améliorer leur lisibilité. Un commentaire est un texte qui n'est pas lu par l'interpréteur Python.

    En Python, un commentaire commence par un dièse (`#!python #`). Tout le reste de la ligne sera ignoré par Python.

    Vous pouvez tester le programme ci-dessous pour voir différents cas de figure et ensuite passer sur l'onglet "Explications" pour comprendre davantage ce qui se passe.
    
    === "Pour tester vous-même"
        {{ IDE("scripts/exemple_commentaires") }}
    === "Explications"

        Vous pouvez cliquer sur les pastilles au bout des lignes pour avoir des explications. 

        ```python
        # Cette ligne n'est pas lu par l'interpréteur (1)
        print("Cette ligne si") # (2)!
        # print("Mais pas celle-ci") (3)

        print("On peut même mettre") # des commentaires en bout de ligne (4)

        "print('On peut utiliser un texte comme un commentaire')" # (5)!
        # Mais c'est moins lisible et il faut penser à alterner " et ' (6)
        ```
        
        1. C'est une ligne qui commence par un `#!python #`, donc elle est complètement ignorée.
        2. Cette ligne ne contient pas de `#!python #`, elle est donc exécutée et provoque l'affichage du message.
        3. Cette ligne commence par un `#!python #` et est donc ignorée.
        4. Le commentaire à la fin de la ligne est ignoré, mais pas l'affichage du message.
        5. Techniquement, ce n'est pas un commentaire, puisque cette expression est évaluée. Mais comme nous l'avons vu avant, l'évaluation d'une expression lors de l'exécution du programme ne provoque pas d'affichage. Utiliser des textes comme commentaires est surtout intéressant lorsqu'on fait des textes sur plusieurs lignes, comme nous le verrons plus tard. C'est un moyen simple de commenter un bloc de code sans avoir à rajouter des `#!python #` devant chaque ligne.
        6. L'avantage des commentaires avec `#!python #`, c'est qu'ils sont facilement identifiables. On sera amené à avoir de nombreux textes dans nos programmes et donc différencier un texte qui sert de commentaire d'un texte qui sert dans une instruction ne sera pas forcément évident. Et si on a des apostrophes dans le commentaire, il faut bien utiliser des guillemets pour encadrer le texte et réciproquement.

    On peut aussi utiliser les commentaires dans le terminal. Cela permet d'expliciter les exemples.

    ```pycon
    >>> 2 + 3  # Un calcul
    5
    >>> "Bonjour"  # Un texte
    'Bonjour'
    ```

    {{ terminal() }}


---
author: Benoît Domart
title: 5. Arithmétique et variables 
---

## Arithmétique

???+ info "Opérations mathématiques"
    Les opérations mathématiques de base s'écrivent ainsi en Python :

    | En math | En Python |
    |:-------:|:---------:|
    | $x+y$   |  `x + y`    |
    | $x-y$   |  `x - y`    |
    | $x\times y$   |  `x * y`    |
    | $x\div{}y$   |  `x / y`    |
    | $x^2$   |  `x**2`    |
    | $x^3$   |  `x**3`    |
    | $x^y$   |  `x**y`    |
    | Reste de la division euclidienne de $x$ par $y$ | `x%y` |
    | Quotient de la division euclidienne de $x$ par $y$ | `x//y` |

    **Python** respecte les priorités de calcul.

    Par exemple, l'expression $\dfrac{5\times(7-4)}{2}$ s'écrit `#!python (5 * (7-4)) / 2`.

    ```pycon
    >>> (5 * (7-4)) / 2
    7.5
    >>> (3 + 4 + 5 + 6)**2
    324
    ```

    {{terminal()}}

???+ info "Nombre décimaux"
    En Python, les nombres décimaux s'écrivent avec un "`.`" à la place de la virgule. Dans l'exemple précédent le premier résultat est décimal.

    Python distingue les nombres entiers et décimaux. Ainsi, `#!python 2.0` est un nombre décimal, alors que `#!python 2` est entier.

    !!! warning "Attention avec les nombres décimaux !!"
        Lorsqu'on utilise des nombres décimaux, il faut faire très attention ! Les calculs sont souvent approximatifs !

        Effectue le calcul suivant : $0.1+0.1+0.1$
        
        {{ terminal() }}


???+ info "Expressions et espaces"
    Dans les exemples précédents, on peut remarquer qu'il y a parfois des espaces dans les expressions et parfois non. Ces espaces sont optionnels. Pour Python, les expressions suivantes sont équivalentes et ne font aucune différence pour l'interpréteur :

    * `#!python 2 + 3 * 4`
    * `#!python 2+3*4`
    * `#!python 2 + 3*4`
    * `#!python 2+3 * 4`
    * `#!python 2` &nbsp;&nbsp;&nbsp;  `#!python +` &nbsp;&nbsp; `#!python 3 *`    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     `#!python 4`

    Les espaces sont vraiment là pour le lecteur humain et n'indiquent en rien les priorités de calcul pour Python. En particulier, `#!python 2 + 3*4` et `#!python 2+3 * 4` donnent bien le même résultat.

    Dans la pratique, on met un espace avant et après chaque opérateur (`+`, `-`, ...) mais qu'en cas de priorité différentes, on ne mettra pas d'espaces autour des opérateurs les plus prioritaires. C'est pourquoi, on préférera `#!python 2 + 3*4` aux autres écritures. 





## Variables

???+ info "Affecter une valeur à une variable"
    Faire un calcul, c'est bien, mais faire quelque chose avec le résultat c'est mieux.

    Pour pouvoir exploiter les résultats calculés, on peut les garder en mémoire. Afin de pouvoir utiliser cette valeur, il faut donner un nom à l'endroit où il se trouve en mémoire. On appelle cela une **variable**. Lorsqu'on associe une valeur à une variable, on dit qu'on **affecte** cette valeur à la variable. Pour faire une **affectation**, il suffit d'écrire : `variable = expression`. 

    ```pycon
    >>> une_variable = 0
    >>> UneAutreVariable = "Bonjour"
    >>> _une_3eme = 7
    >>> _4_fois_3 = 12
    ```

    Là aussi, les espaces autour de `=` sont optionnels mais conseillés pour plus de lisibilité.

???+ info "Syntaxe des noms de variables"
    On ne peut pas écrire n'importe quoi pour le nom d'une variable. En Python, ce nom doit respecter les règles suivantes :

    - commencer par une lettre ou un `_` ;
    - contenir des lettres, des chiffres et des `_`.

???+ exercice "Exercice 3"
    Parmi les noms suivants, déterminer ceux qui sont des noms valides de variables en Python.

    === "Propositions"

        - [ ] `variable`
        - [ ] `une variable`
        - [ ] `apollo13`
        - [ ] `S_N_T`
        - [ ] `2fois3`                
        - [ ] `vOITure`


    === "Solution"

        - :white_check_mark: `variable`
        - :x: `une variable` : Il y a un espace, ce qui est interdit.
        - :white_check_mark: `apollo13`
        - :white_check_mark: `S_N_T`
        - :x: `2fois3` : Une variable ne peut pas commencer par un chiffre.
        - :white_check_mark: `vOITure`

???+ exercice "Exercice 4 (répondre sur la feuille)"
    Parmi les noms suivants, déterminer ceux qui sont des noms valides de variables en Python.

    - [ ] `2_de`
    - [ ] `Lycee3Sources`
    - [ ] `S N T`
    - [ ] `trois*rien`        


???+ info "Utilisation d'une variable"
    Pour obtenir la valeur d’une variable il faut juste taper son nom dans le terminal. On peut aussi utiliser la fonction `#!python print` :

    ```pycon
    >>> a = 1
    >>> a
    1
    >>> print(a)
    1
    ```

    Une fois la variable affectée, on peut l’utiliser dans des opérations.

    ```pycon
    >>> a + 2
    3
    >>> b = a + 1
    >>> b
    2
    ```

    Il est aussi possible de modifier la valeur d’une variable.

    ```pycon
    >>> a = 2
    >>> a = 5
    >>> a
    5
    ```

???+ info "Utiliser la valeur précédente d'une variable pour calculer sa nouvelle valeur"
    On peut utiliser la valeur actuelle d’une variable pour lui en donner une nouvelle.

    ```pycon
    >>> a = 5
    >>> a = a + 1
    >>> a
    6
    >>> a = a + 1
    >>> a
    7
    ```

    Il faut donc bien penser que la partie de droite de l'affectation est évaluée avant de faire l'affectation. Après la première affectation `a` vaut `5`. Pour la deuxième affectation, l'interpréteur commence par calculer `a+1`, qui vaut `6`, et l'affecte ensuite à `a` qui vaut donc `6`.

    !!! warning "Attention !"
        Le symbole " $=$ " n'a donc pas la même signification qu'en mathématiques. En effet, un nombre $a$ n'est jamais égal à $a+1$.

        Lorsqu'on écrit `a=a+1`, il faut lire :
        *À la variable `a`, on affecte la valeur : "ancienne valeur de `a`" plus 1.*

???+ exercice "Exercice 5 (répondre sur la feuille)"
    Déterminer la valeur de `b` à la fin de cette séquences d'instructions. Vous pouvez demander à Python de vous afficher le résultat.

    {{ IDE("scripts/exercice_affectations_operations") }}

    ??? success "Solution"
        Pour ce genre d'exercices, il peut être utile de faire ce qu'on appelle un **tableau de suivi** : c'est un tableau qui permet de connaître le contenu de chaque variable après chaque instruction du programme (c'est-à-dire chaque ligne) :

        | n° ligne | a | b |
        |:-------:|:---------:|:---------:|
        | 1 | 9  | -  |
        | 2 | 9  | 2  |
        | 3 | 11 | 2  |
        | 4 | 11 | 22 |



???+ info "Affectations et opérations simultanées"
    Il existe des raccourcis pour ajouter ou enlever une valeur à une variable.

    ```pycon
    >>> a += 1  # a = a + 1
    >>> b -= 5  # b = b - 5
    ```

    De la même façon, il existe des raccourcis pour multiplier ou diviser la valeur d'une variable par un nombre.

    ```pycon
    >>> c /= 2  # c = c / 2
    >>> d *= 5  # d = d * 5
    ```

???+ info "Variables et casse"
    Les noms en Python sont sensibles à la **casse**. Cela veut dire que Python fait la différence entre majuscule et minuscule. Ainsi `nom`, `NOM`, `Nom` ou `nOm` sont considérées comme 4 variables différentes.

???+ warning "Erreur de variable"
    Lorsqu'on se trompe sur le nom d'une variable, ou qu'on essaie d'utiliser sa valeur avant de lui en avoir affecté une, on obtient un message d'erreur : `NameError: name 'varialbe_inconnue' is not defined`. 

    Cela veut dire que le nom, ici `varialbe_inconnue`, ne correspond à aucun nom connu par Python pour la session actuelle. En général, c'est qu'on a mal écrit le nom de la variable. Dans cet exemple, c'était peut-être `variable_inconnue` que l'ont voulait écrire.
 

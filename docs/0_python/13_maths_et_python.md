---
author: Benoît Domart
title: 13. Quelques exercices de Python
---

???+ exercice "Quelques exercices de Python"

    Voici quelques exercices de Python :

    4. [Fonctions simples](https://codex.forge.apps.education.fr/exercices/fonctions/){target=_blank}
    1. [Fonctions affines](https://codex.forge.apps.education.fr/exercices/fonctions_affines/){target=_blank}
    4. [Somme de deux entiers](https://codex.forge.apps.education.fr/exercices/somme/){target=_blank}
    4. [Consommation d'énergie](https://codex.forge.apps.education.fr/exercices/consommation_electrique/){target=_blank}
    4. [Autour de range](https://codex.forge.apps.education.fr/exercices/range/){target=_blank}
    2. [Géométrie analytique](https://codex.forge.apps.education.fr/exercices/geometrie_vectorielle/){target=_blank}
    3. [Différentes distances](https://codex.forge.apps.education.fr/exercices/distances/){target=_blank}

import os

os.chdir('/home/ben/Documents/GIT_Projets/domart/snt/docs/DS/DS4/images/')

######################

image = open('ds4-sujet1.1.pbm', 'w') 

image.write('P1\n')
image.write('21 21\n')

for ligne in range(21):
    for colonne in range(21):
        if ligne==colonne:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()


######################

image = open('ds4-sujet1.2.pbm', 'w') 

image.write('P1\n')
image.write('21 21\n')

for ligne in range(21):
    for colonne in range(21):
        if ligne==colonne or ligne-colonne == 1 or ligne-colonne == 2:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('ds4-sujet1.3.pbm', 'w') 

image.write('P1\n')
image.write('21 21\n')

for ligne in range(21):
    for colonne in range(21):
        if ligne==colonne and ligne%2 == 0:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('ds4-sujet1.4.pbm', 'w') 

image.write('P1\n')
image.write('21 21\n')

for ligne in range(21):
    for colonne in range(21):
        if ligne==0 or colonne == 0 or ligne == 20 or colonne == 20:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('ds4-sujet1.5.pbm', 'w') 

image.write('P1\n')
image.write('21 21\n')

for ligne in range(21):
    for colonne in range(21):
        if ligne<=6 or ligne >= 14:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()

######################

image = open('ds4-sujet1.6.pbm', 'w') 

image.write('P1\n')
image.write('58 20\n')

for ligne in range(20):
    for colonne in range(58):
        if ligne == colonne or ligne+colonne == 38 or ligne == colonne - 38:
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()
---
author: "Benoît Domart"
title: "DS n°4"
---

Utiliser le code suivant pour générer un fichier image au format PBM :

```py
image = open('....pbm', 'w') #(1)!

image.write('P1\n')
image.write('... ...\n')#(2)!

for ligne in range(...):
    for colonne in range(...):
        if ...:#(3)!
            image.write('1 ')
        else:
            image.write('0 ')
    image.write('\n')

image.close()
```

1. Remplacer les `...` par le nom du fichier.
2. Remplacer les `...` par le nombre de colonnes puis le nombre de lignes.
3. Indiquer dans quel cas on met un `0` (blanc), et dans quel cas on met un `1` (noir).


???+exercice "Exercice 1"

    [Lien vers l'exercice 1.](https://capytale2.ac-paris.fr/web/c/2fbf-3331742){target=_blank}

???+exercice "Exercice 2"

    [Lien vers l'exercice 2.](https://capytale2.ac-paris.fr/web/c/5a6b-3331886){target=_blank}


???+exercice "Exercice 3"

    [Lien vers l'exercice 3.](https://capytale2.ac-paris.fr/web/c/5ffa-3331893){target=_blank}

???+exercice "Exercice 4"

    [Lien vers l'exercice 4.](https://capytale2.ac-paris.fr/web/c/1ba5-3331904){target=_blank}

???+exercice "Exercice 5"

    [Lien vers l'exercice 5.](https://capytale2.ac-paris.fr/web/c/7458-3331909){target=_blank}

???+exercice "Exercice 6"

    [Lien vers l'exercice 6.](https://capytale2.ac-paris.fr/web/c/9fab-3331915){target=_blank}
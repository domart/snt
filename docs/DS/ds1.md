---
author: "Benoît Domart"
title: "DS n°1"
---

???+exercice "Exercice 1"
    [L'énoncé](https://capytale2.ac-paris.fr/web/c/1c4c-2214726){target=_blank}

???+exercice "Exercice 2"
    [L'énoncé](https://capytale2.ac-paris.fr/web/c/36a6-2214760){target=_blank}

???+exercice "Exercice 3"
    [L'énoncé](https://capytale2.ac-paris.fr/web/c/d0b1-2214963){target=_blank}

???+exercice "Exercice 4"
    [L'énoncé](https://capytale2.ac-paris.fr/web/c/6bbd-2215202){target=_blank}

???+exercice "Exercice 5"
    [L'énoncé](https://capytale2.ac-paris.fr/web/c/42e2-4602635){target=_blank}
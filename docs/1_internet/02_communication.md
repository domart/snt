---
author: Benoît Domart
title: 2. Communication entre deux machines 
---

???+exercice "Quiz sur la communication entre deux machines"
    <iframe 
        title=""
        width="1088"
        height="500"
        frameborder="0"
        allowfullscreen="allowfullscreen"
        src="../h5p/Internet_s2_Communication_entre_deux_machines.html?embed=true">
    </iframe>
    <script src="../../xtra/javascripts/h5p-resizer.js" charset="UTF-8"></script>

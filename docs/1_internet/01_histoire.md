---
author: Benoît Domart
title: 1. Histoire d'internet 
---

## 1. Quiz sur l'histoire d'internet.

???+exercice "Exercice 1 - Quiz sur l'histoire d'internet"
    <iframe 
        title="Quiz sur l'histoire d'internet"
        width="1088"
        height="500"
        frameborder="0"
        allowfullscreen="allowfullscreen"
        src="../h5p/internet_s1_histoire-v3.html?embed=true">
    </iframe><script src="https://app.Lumi.education/api/v1/h5p/core/js/h5p-resizer.js" charset="UTF-8"></script>

## 2. Les câbles sous-marins.

???+exercice "Exercice 2 - Les câbles sous-marins"
    Aller voir cette carte : [https://www.submarinecablemap.com/](https://www.submarinecablemap.com/){target=_blank}.

    1. Que représente-t-elle ?
    2. Où semblent être effectués les principaux échanges d’informations par internet ?
    3. Quelle est la longueur du câble le plus long que tu as trouvé ?

    **Note tes réponses dans l'activité 2 du cours.**